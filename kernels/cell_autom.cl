typedef struct {
    double terrain_height;
    double water_height;
    double sediment; // suspended sediment amount

    // outflow flux
    double flux_N;
    double flux_S;
    double flux_W;
    double flux_E;

    // velocity vector
    double velocity_x;
    double velocity_y;
} cell;

__kernel void cell_update(__global const cell *grid, __global cell *return_grid, __global double *debug_output) {
    int x = get_global_id(0);
    int y = get_global_id(1);

    int x_max = get_global_size(0) - 1;
    int y_max = get_global_size(1) - 1;

    // stałe póki co tutaj
    double delta_N = 1;
    double pipe_area_C = 1;
    double pipe_Wength_C = 1;
    double gravity_const_C = 9.8;
    double dist_X = 1, dist_Y = 1; // odległość między kolejnymi punktami na siatce, im mniejsza tym większa rozdzielczość symulacji
    double sediment_capacity_C = 0.1;
    double sediment_dissolving_C = 1;
    // stałe

    cell cell_default;
    cell_default.sediment = 0;
    cell_default.terrain_height = 0;
    cell_default.water_height = 0;
    cell_default.sediment = 0;
    cell_default.flux_N = 0;
    cell_default.flux_S = 0;
    cell_default.flux_W = 0;
    cell_default.flux_E = 0;
    cell_default.velocity_x = 0;
    cell_default.velocity_y = 0;

    cell cell_current = grid[x + y * get_global_size(0)];
    cell cell_N = cell_default;
    cell cell_S = cell_default;
    cell cell_W = cell_default;
    cell cell_E = cell_default;
    double flux_N_updated = 0;
    double flux_S_updated = 0;
    double flux_W_updated = 0;
    double flux_E_updated = 0;


// ###############STEP 1########################

    // output flux values have to be positive since gravity doesn't work upside down
    // output flux has to be 0 at the border to avoid water leaking over the grid
    // output flux scaling factor has to be used to avoid negative water level after update
    if(y - 1 >= 0){
        cell_N = grid[x + (y - 1) * get_global_size(0)];

        double delta_h = cell_current.terrain_height + cell_current.water_height - cell_N.terrain_height - cell_N.water_height;
        flux_N_updated = fmax(cell_current.flux_N + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0); 
    }
    if(y + 1 <= y_max){
        cell_S = grid[x + (y + 1) * get_global_size(0)];

        double delta_h = cell_current.terrain_height + cell_current.water_height - cell_S.terrain_height - cell_S.water_height;
        flux_S_updated = fmax(cell_current.flux_S + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0); 
    }
    if(x - 1 >= 0){
        cell_W = grid[(x - 1) + y * get_global_size(0)];

        double delta_h = cell_current.terrain_height + cell_current.water_height - cell_W.terrain_height - cell_W.water_height;
        flux_W_updated = fmax(cell_current.flux_W + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0); 
    } 
    if(x + 1 <= x_max){
        cell_E = grid[(x + 1) + y * get_global_size(0)];

        double delta_h = cell_current.terrain_height + cell_current.water_height - cell_E.terrain_height - cell_E.water_height;
        flux_E_updated = fmax(cell_current.flux_E + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0); 
    }
    
    // update outflow flux values
    double flux_out_sum = flux_N_updated + flux_S_updated + flux_W_updated + flux_E_updated;
    double flux_scaling_factor = 1;
    if(flux_out_sum > cell_current.water_height){
        flux_scaling_factor = (cell_current.water_height * dist_X * dist_Y)/flux_out_sum;
        if(flux_scaling_factor > 1) flux_scaling_factor = 1; // fmin nie działa z jakiegoś powodu...
    }

    flux_N_updated = flux_N_updated * flux_scaling_factor;
    flux_S_updated = flux_S_updated * flux_scaling_factor;
    flux_W_updated = flux_W_updated * flux_scaling_factor;
    flux_E_updated = flux_E_updated * flux_scaling_factor;


    // // update cell after step 1
    // grid[x + y * get_global_size(0)].flux_N = flux_N_updated;
    // grid[x + y * get_global_size(0)].flux_S = flux_S_updated;
    // grid[x + y * get_global_size(0)].flux_E = flux_E_updated;
    // grid[x + y * get_global_size(0)].flux_W = flux_W_updated;

    cell cell_updated;
    // if(terrain_height_updated < 0) terrain_height_updated = 0; // tymczasowo, proces depozycji powinien to ogarnac
    cell_updated.terrain_height = cell_current.terrain_height;
    cell_updated.water_height = cell_current.water_height;// water_height_updated;
    cell_updated.sediment = cell_current.sediment;
    cell_updated.flux_N = flux_N_updated;
    cell_updated.flux_S = flux_S_updated;
    cell_updated.flux_W = flux_W_updated;
    cell_updated.flux_E = flux_E_updated;
    cell_updated.velocity_x = cell_current.velocity_x;
    cell_updated.velocity_y = cell_current.velocity_y;
    return_grid[x + y * get_global_size(0)] = cell_updated;

    /** DEBUG */ 
    // debug_output[0] = velocity_val;
    // debug_output[1] = velocity_y;
    // debug_output[2] = water_height_d1_d2_avg * dist_X;
    // debug_output[3] = terrain_height_updated;
    // debug_output[4] = sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
    // debug_output[5] = sediment_transport_capacity;
}

__kernel void step2(__global const cell *grid, __global cell *return_grid, __global double *debug_output) {

        // stałe póki co tutaj
    double delta_N = 1;
    double pipe_area_C = 1;
    double pipe_Wength_C = 1;
    double gravity_const_C = 9.8;
    double dist_X = 1, dist_Y = 1; // odległość między kolejnymi punktami na siatce, im mniejsza tym większa rozdzielczość symulacji
    double sediment_capacity_C = 10;
    double sediment_dissolving_C = 1;
// STEP 2 / kolejna iteracja bo póki co to same zera bedo

    int x = get_global_id(0);
    int y = get_global_id(1);

    int x_max = get_global_size(0) - 1;
    int y_max = get_global_size(1) - 1;

    cell cell_current = grid[x + y * get_global_size(0)];

    cell cell_default;
    cell_default.sediment = 0;
    cell_default.terrain_height = 0;
    cell_default.water_height = 0;
    cell_default.sediment = 0;
    cell_default.flux_N = 0;
    cell_default.flux_S = 0;
    cell_default.flux_W = 0;
    cell_default.flux_E = 0;
    cell_default.velocity_x = 0;
    cell_default.velocity_y = 0;

    cell cell_N = cell_default;
    cell cell_S = cell_default;
    cell cell_W = cell_default;
    cell cell_E = cell_default;

    if(y - 1 >= 0){
        cell_N = grid[x + (y - 1) * get_global_size(0)];
    }
    if(y + 1 <= y_max){
        cell_S = grid[x + (y + 1) * get_global_size(0)];
    }
    if(x - 1 >= 0){
        cell_W = grid[(x - 1) + y * get_global_size(0)];
    } 
    if(x + 1 <= x_max){
        cell_E = grid[(x + 1) + y * get_global_size(0)];
    }

    // update water basing on out flux and in flux
    double flux_in_sum = cell_N.flux_S + cell_S.flux_N + cell_W.flux_E + cell_E.flux_W;
    double flux_out_sum = cell_current.flux_N + cell_current.flux_S + cell_current.flux_W + cell_current.flux_E;
    double water_height_delta = flux_in_sum - flux_out_sum; // 1 - 1 = 5.96046e-08
    double water_height_updated = cell_current.water_height + water_height_delta/(dist_X * dist_Y); // divided by lx * ly (distance between grid points)

    // calculate velocity vector
    // symulacja jest kilkukrokowa, pozniej trzeba to sensownie rozdzielic
    double water_height_d1_d2_avg = (water_height_updated + cell_current.water_height)/2;

    double water_x_delta = cell_W.flux_E - cell_current.flux_W + cell_current.flux_E - cell_E.flux_W;
    double velocity_x = water_x_delta/(water_height_d1_d2_avg * dist_Y);

    double water_y_delta = cell_N.flux_S - cell_current.flux_N + cell_current.flux_S - cell_S.flux_N;
    double velocity_y = water_y_delta/(water_height_d1_d2_avg * dist_X);

    // erosion and deposition
    double local_tilt_angle = 0.785398; // 45 deg, narazie hardcoded, później się to rozkmini (to trzeba bedzie updatowac co iteracje)

    // calculate local tilt
    double dh_dx = (cell_W.terrain_height - cell_E.terrain_height)/2;
    double dh_dy = (cell_N.terrain_height - cell_S.terrain_height)/2;
    double sin_alfa = sqrt(dh_dx*dh_dx + dh_dy*dh_dy)/sqrt(1 + dh_dx*dh_dx + dh_dy*dh_dy);

    double velocity_val = sqrt(velocity_y*velocity_y + velocity_x*velocity_x);
    // double sediment_transport_capacity = sediment_capacity_C * sin_alfa * velocity_val;
        double sediment_transport_capacity = sediment_capacity_C * sin(local_tilt_angle) * velocity_val;


    double terrain_height_updated = cell_current.terrain_height;
    double sediment_updated = 0;
    // decide if deposit or erode
    if(sediment_transport_capacity > cell_current.sediment) {
        terrain_height_updated = cell_current.terrain_height - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
        sediment_updated = cell_current.sediment + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
    } else {
        //deposit sediment
        terrain_height_updated = cell_current.terrain_height + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
        sediment_updated = cell_current.sediment - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
    }

    cell cell_updated;
    if(terrain_height_updated < 0) terrain_height_updated = 0; // tymczasowo, proces depozycji powinien to ogarnac
    cell_updated.terrain_height = terrain_height_updated;
    cell_updated.water_height = cell_current.water_height;// water_height_updated;
    cell_updated.sediment = sediment_updated;
    cell_updated.flux_N = cell_current.flux_N;
    cell_updated.flux_S = cell_current.flux_S;
    cell_updated.flux_W = cell_current.flux_W;
    cell_updated.flux_E = cell_current.flux_E;
    cell_updated.velocity_x = velocity_x;
    cell_updated.velocity_y = velocity_y;
    return_grid[x + y * get_global_size(0)] = cell_updated;

    // sediment transport

}