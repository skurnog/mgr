// STEP 2 / kolejna iteracja bo póki co to same zera bedo
    // update water basing on out flux and in flux
    double flux_in_sum = cell_N.flux_S + cell_S.flux_N + cell_W.flux_E + cell_E.flux_W;
    double water_height_delta = flux_in_sum - flux_out_sum * flux_scaling_factor; // 1 - 1 = 5.96046e-08
    double water_height_updated = cell_current.water_height + water_height_delta/(dist_X * dist_Y); // divided by lx * ly (distance between grid points)

    // calculate velocity vector
    // symulacja jest kilkukrokowa, pozniej trzeba to sensownie rozdzielic
    double water_height_d1_d2_avg = (water_height_updated + cell_current.water_height)/2;

    double water_x_delta = cell_W.flux_E - cell_current.flux_W + cell_current.flux_E - cell_E.flux_W;
    double velocity_x = water_x_delta/(water_height_d1_d2_avg * dist_Y);

    double water_y_delta = cell_N.flux_S - cell_current.flux_N + cell_current.flux_S - cell_S.flux_N;
    double velocity_y = water_y_delta/(water_height_d1_d2_avg * dist_X);

    // erosion and deposition
    double local_tilt_angle = 0.785398; // 45 deg, narazie hardcoded, później się to rozkmini (to trzeba bedzie updatowac co iteracje)
    double velocity_val = sqrt(velocity_y*velocity_y + velocity_x*velocity_x);
    double sediment_transport_capacity = sediment_capacity_C * sin(local_tilt_angle) * velocity_val;

    double terrain_height_updated = cell_current.terrain_height;
    double sediment_updated = 0;
    // decide if deposit or erode
    if(sediment_transport_capacity > cell_current.sediment) {
        terrain_height_updated = cell_current.terrain_height - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
        sediment_updated = cell_current.sediment + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
    } else {
        //deposit sediment
        terrain_height_updated = cell_current.terrain_height + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
        sediment_updated = cell_current.sediment - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
    }

    // sediment transport

