double fade(double t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

double lerp(double t, double a, double b)
{
    return a + t * (b - a);
}

double grad(int hash, double x, double y, double z)
{
    int h = hash & 15;
    double u = h<8 ? x : y;
    double v = h<4 ? y : h==12||h==14 ? x : z;
    return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

__kernel void perlin(__global int *p, double scale, __global float *result_bitmap)
{
    int x_size = get_global_size(0);
    int y_size = get_global_size(1);

	double x = (get_global_id(0)/(double)x_size)*scale;
    double y = (get_global_id(1)/(double)y_size)*scale;
    double z = 0;

    // Find the unit cube that the point is located in.
    int X = (int)x & 255;
    int Y = (int)y & 255;
    int Z = (int)z & 255;

    // Find point location relatively to the cube.
    double xd = x - X;
    double yd = y - Y;
    double zd = z - Z;

    // Compute fade curves.
    double u = fade(xd);
    double v = fade(yd);
    double w = fade(zd);

    int A = p[X] + Y;
    int AA = p[A] + Z;
    int AB = p[A + 1] + Z;

    int B = p[X + 1] + Y;
    int BA = p[B] + Z;
    int BB = p[B + 1] + Z;

    double retval = lerp(w, lerp(v, lerp(u, grad(p[AA], xd, yd, zd),
                                   grad(p[BA], xd - 1, yd, zd)),
                            lerp(u, grad(p[AB], xd, yd - 1, zd),
                                   grad(p[BB], xd - 1, yd - 1, zd))),
                    lerp(v, lerp(u, grad(p[AA + 1], xd, yd, zd - 1),
                                   grad(p[BA + 1], xd - 1, yd, zd - 1)),
                            lerp(u, grad(p[AB + 1], xd, y - 1, zd - 1),
                                   grad(p[BB + 1], xd - 1, yd - 1, zd - 1))));

    result_bitmap[get_global_id(0) + get_global_id(1) * get_global_size(0)] = (retval+1)/2;
}