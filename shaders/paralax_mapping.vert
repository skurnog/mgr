#version 130

in vec3 position;
//in vec3 normal;
in vec2 texCoord;
uniform vec3 eyePos;
//in vec3 tangent;
//in vec3 bitangent;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

out vec2 fragTexCoor;
out vec3 fragTanEyePos;
out vec3 fragTanLightPos;
out vec3 fragTanFragPos;
out vec3 fragTanNormal;

//const vec3 lightPos = vec3(0f, 1.5f, 2f);
//const vec3 eyePos = vec3(0.0f, 0.0f, 1.0f);
const vec3 normal = vec3(0,0,1);
const vec3 tangent = vec3(1,0,0);
const vec3 bitangent = vec3(0,1,0);

void main()
{
    mat4 pvm_matrix = projectionMat * viewMat * modelMat;
    vec3 fragPos = vec3(modelMat * vec4(position, 0.0));

    vec3 T = normalize(vec3(modelMat * vec4(tangent, 0.0)));
    vec3 B = normalize(vec3(modelMat * vec4(bitangent, 0.0)));
    vec3 N = normalize(vec3(modelMat * vec4(normal, 0.0)));
    mat3 TBN = transpose(mat3(T, B, N));

    fragTanEyePos   = normalize(TBN * (eyePos - fragPos));
    fragTanFragPos  = TBN * normalize(fragPos);
    fragTexCoor = texCoord;

    gl_Position = pvm_matrix * vec4(position, 1.0);
}
