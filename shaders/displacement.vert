#version 130

in vec3 position;
in lowp vec2 TexCoord;
out lowp vec2 vTexCord;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

uniform sampler2D textureHeightmap;

void main()
{
    mat4 pvm_matrix = projectionMat * viewMat * modelMat;

    float h = texture2D(textureHeightmap, TexCoord).r * 0.3;
    vTexCord = TexCoord;
    gl_Position = pvm_matrix * vec4(position.xy, h, 1.0);
}
