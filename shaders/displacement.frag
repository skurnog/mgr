#version 130

uniform sampler2D textureHeightmap;
uniform sampler2D textureNormalmap;
uniform sampler2D textureColor;
in lowp vec2 vTexCord;
out vec4 color;

void main()
{
    color = texture2D(textureColor, vTexCord);
}
