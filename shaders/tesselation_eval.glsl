#version 400

layout(quads, fractional_odd_spacing, ccw) in;
out vec2 texCoord;
out float depth;

uniform sampler2D depthMap;
//uniform mat4 mvp;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;
mat4 mvp = modelMat * viewMat * projectionMat;

void main(){
    float u = gl_TessCoord.x;
    float v = gl_TessCoord.y;

    vec4 a = mix(gl_in[1].gl_Position, gl_in[0].gl_Position, u);
    vec4 b = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, u);
    vec4 position = mix(a, b, v);
    texCoord = position.xy;
    float height = texture(depthMap, texCoord).r;
    gl_Position = mvp * vec4(texCoord, height, 1.0);
    depth = gl_Position.z;
}