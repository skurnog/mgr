#version 130

in lowp vec3 position;
in lowp vec2 texCoord;
out lowp vec2 TexCord;
out lowp vec3 FragPos;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

void main()
{
    TexCord = texCoord;
    FragPos = vec3(modelMat * vec4(position, 1.0f));
    gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1.0);
}
