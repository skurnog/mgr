#version 400

in vec2 vTexCoord;
out vec4 color;

uniform sampler2D textureHeightmap;
uniform sampler2D textureNormalmap;
uniform sampler2D textureColor;

void main()
{
    color = texture2D(textureColor, vTexCoord);
//color = vec4(0.3, 0.4, 0.4, 1.0);
}
