#version 400

in vec3 position;
uniform sampler2D textureDepth;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;


void main(void){
    vec2 texcoord = position.xy;
    float height = texture(textureDepth, texcoord).r;
    vec4 displaced = vec4(position.x, position.y, height, 1.0);
    gl_Position = displaced;
//    gl_Position = projectionMat * viewMat * modelMat * vec4(position, 1);
}
