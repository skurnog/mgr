#version 130

in vec2 fragTexCoor;
in vec3 fragTanEyePos;
in vec3 fragTanLightPos;
in vec3 fragTanFragPos;
in vec3 fragTanNormal;

uniform sampler2D textureNormal;
uniform sampler2D textureDepth;
uniform sampler2D textureColor;

const float uHeightScale = 0.5;
const float uMinLayers = 1;
const float uMaxLayers = 5;

out vec4 color;

vec2 ParallaxMapping_Steep2(vec2 texCoords, vec3 viewDir)
{
    float height_scale = uHeightScale;
    const float minLayers = uMaxLayers;
    const float maxLayers = uMinLayers;

    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    float layerDepth = 1.0 / numLayers;
    float currentLayerDepth = 0.0;
    vec2 P = viewDir.xy / viewDir.z * height_scale;
    vec2 deltaTexCoords = P / numLayers;

    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(textureDepth, currentTexCoords).r;

    int cnt = 0;
    while(currentLayerDepth < currentDepthMapValue && cnt < numLayers)
    {
        currentTexCoords -= deltaTexCoords;
        currentDepthMapValue = texture(textureDepth, currentTexCoords).r;
        currentLayerDepth += layerDepth;

        cnt++;
    }

    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(textureDepth, prevTexCoords).r - currentLayerDepth + layerDepth;

    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = mix(currentTexCoords, prevTexCoords, weight);

    return finalTexCoords;
}

void main()
{
    vec3 viewDir = normalize(fragTanEyePos - fragTanFragPos);
    vec2 texCoor = ParallaxMapping_Steep2(fragTexCoor, viewDir);

    if (texCoor.x < 0 || texCoor.x > 1 || texCoor.y < 0 || texCoor.y > 1) discard;

    vec3 diffuse = texture(textureColor, texCoor).rgb;

    color = vec4(diffuse, 1.0);
}
