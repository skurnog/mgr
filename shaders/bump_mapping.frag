#version 130

uniform sampler2D textureSampler;
in lowp vec2 TexCord;
in lowp vec3 FragPos;
out lowp vec4 color;

const vec2 size = vec2(2.0,0.0);
const ivec3 off = ivec3(-1,0,1);

void main()
{
    vec4 height_val = texture(textureSampler, TexCord);

    float s11 = height_val.x;
    float s01 = textureOffset(textureSampler, TexCord, off.xy).x * 5;
    float s21 = textureOffset(textureSampler, TexCord, off.zy).x * 5;
    float s10 = textureOffset(textureSampler, TexCord, off.yx).x * 5;
    float s12 = textureOffset(textureSampler, TexCord, off.yz).x * 5;
    vec3 va = normalize(vec3(size.xy,s21-s01));
    vec3 vb = normalize(vec3(size.yx,s12-s10));
    vec4 bump = vec4(cross(va,vb), s11);

    vec3 lightDir = normalize(vec3(2f, 1.5f, 2f) - vec3(FragPos.x, FragPos.y, s11)); // tu jeszcze był lightdir
    float diffuseIntensity = max(dot(vec3(bump), lightDir), 0.0);

    color = vec4(0.5, 0.2, 0.4, 1.0) * diffuseIntensity;
}
