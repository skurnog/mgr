#version 400

in vec3 position;
out vec3 vPosition;

in vec2 texCoord;
out vec2 vTexCoord;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

uniform sampler2D textureHeightmap;
uniform sampler2D textureNormalmap;
uniform sampler2D textureColor;

void main()
{
    mat4 pvm_matrix = projectionMat * viewMat * modelMat;

    float height = texture2D(textureHeightmap, texCoord).r;
    vPosition = vec3(position.xy, height);
    vTexCoord = texCoord;
}
