**Windows**: nie pamiętam dokładnie ale chyba instalacja nvidiowych CUDA rzeczy wystarcza...  
**Linux**: https://wiki.archlinux.org/index.php/GPGPU potem **reboot**

**wxWidgets**  
- On those platforms which use a configure script (e.g. Linux and Mac OS) OpenGL support is automatically enabled if the relative headers and libraries are found. To switch it on under the other platforms (e.g. Windows), you need to edit the setup.h file and set wxUSE_GLCANVAS to 1 and then also pass USE_OPENGL=1 to the make utility. You may also need to add opengl32.lib and glu32.lib to the list of the libraries your program is linked with.  
- To build wxWidgets with the standard containers you need to set wxUSE_STD_CONTAINERS option to 1 in wx/msw/setup.h for wxMSW builds or specify –enable-std_containers option to configure (which is also implicitly enabled by –enable-stl option) in Unix builds.  