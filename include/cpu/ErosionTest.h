// Created by Norbert Skurnóg on 20.05.18.
#ifndef MGR_EROSIONTEST_H
#define MGR_EROSIONTEST_H

#include <vector>

class Heightmap;
class Cell;
class ErosionTest
{
public:
    static std::vector<Cell> run(const Heightmap& inputHeightmap);

private:
    static size_t grid_w;
    static size_t grid_h;
    static size_t x_max;
    static size_t y_max;

    static void run_step(Cell* input_grid, Cell* output_grid);
    static void step1(Cell* input_grid, Cell* output_grid);
};

#endif //MGR_EROSIONTEST_H
