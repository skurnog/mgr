// Created by Norbert Skurnóg on 29.08.18.


#ifndef MGR_IMAGE_T_H
#define MGR_IMAGE_T_H

#include <vector>

struct image_t {
    std::vector<unsigned char> image_bytes; //the raw pixels
    unsigned width, height;
};


#endif //MGR_IMAGE_T_H
