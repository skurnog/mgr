// Created by Norbert Skurnóg on 21.03.18.

#ifndef MGR_IMAGE_H
#define MGR_IMAGE_H

#include <vector>
#include <string>
#include <epoxy/gl.h>

struct Cell;

class Heightmap {
public:
    Heightmap(const std::string& heightmap_path, const std::string& normalmap_path = "", const std::string& colormap_path = "");
    void setHeights(const std::vector<Cell>&);
    void loadWatermap(const std::string& filePath);

    const std::vector<unsigned char>& getHeights() const { return heights; }
    const std::vector<unsigned char>& getWaterHeights() const { return water_heights; }
    std::vector<unsigned char> getHeightmapTexture() { return heightmap_bytes; }
    std::vector<unsigned char> getNormalmapTexture() { return normalmap_bytes; }
    std::vector<unsigned char> getColorTexture() { return colormap_bytes; }
    size_t getWidth() const;
    size_t getHeight() const;
    size_t getCellsCount() const;

private:
    size_t width;
    size_t height;

    std::vector<unsigned char> heightmap_bytes;
    std::vector<unsigned char> watermap_bytes;
    std::vector<unsigned char> normalmap_bytes;
    std::vector<unsigned char> colormap_bytes;

    std::vector<unsigned char> heights;
    std::vector<unsigned char> water_heights;

    void heighmap_to_heights();
    void watermap_to_water_heights();
};


#endif //MGR_IMAGE_H
