//
// Created by norbert on 21.03.18.
//

#ifndef MGR_RESOURCEMANAGER_H
#define MGR_RESOURCEMANAGER_H

#include <string>
#include <memory>
#include <vector>

#include <resources/image_t.h>

// todo exceptions

class Heightmap;

class ResourceManager {
    using string = std::string;
public:
    static image_t loadImage(const string& file_path);
    static void saveImage(const string& file_path, const image_t& image);
};


#endif //MGR_RESOURCEMANAGER_H
