//
// Created by norbert on 4/7/18.
//

#ifndef MGR_OPENGL_H
#define MGR_OPENGL_H

#include <string>
#include <exception>
#include <map>

namespace except {
    enum class ErrorType {
        DEFAULT, SHADER, DRAW
    };

    const std::map<ErrorType, std::string> errorDescriptions = {
            {ErrorType::DEFAULT, ""},
            {ErrorType::SHADER,  "Problem with shader occurred"},
            {ErrorType::SHADER,  "Problem while drawing occurred"},
    };

    class OpenGL: public std::exception {
    public:
        OpenGL(ErrorType error_type, unsigned error_code, const std::string &additional_info = "unknown");
        virtual const char *what();

    private:
        std::string what_str;
    };
}

#endif //MGR_OPENGL_H
