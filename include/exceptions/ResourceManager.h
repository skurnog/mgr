//
// Created by norbert on 27.03.18.
// Exception class for issues related to resource loading
//

#ifndef MGR_EXCEPTIONRESOURCELOADER_H
#define MGR_EXCEPTIONRESOURCELOADER_H

#include <exception>
#include <string>

namespace except {
    class ResourceManager: public std::exception {
        using string = std::string;
    public:
        ResourceManager(unsigned error_code, const string& error_message, const string& additional_info = "");
        virtual const char* what();
    private:
        string what_str;
    };

}

#endif //MGR_EXCEPTIONRESOURCELOADER_H
