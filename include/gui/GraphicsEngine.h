#ifndef GRAPHICSENGINE_H
#define GRAPHICSENGINE_H

#include <memory>

#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>

class ShaderProgram;
class GameEngine;
class Window;
class Scene;

class GraphicsEngine {
public:
    GraphicsEngine(Window* window);
    GraphicsEngine() = delete;
    ~GraphicsEngine();

    void nextFrame();

    std::shared_ptr<Scene> getScene() const noexcept;
private:
    Window* window;
    int window_width;
    int window_height;

    std::unique_ptr<ShaderProgram> mainShader;
    std::shared_ptr<Scene> scene;

    // todo remove these, to scene/model/camera
    int sh_uni_model;
    int sh_uni_view;
    glm::mat4 projection_mat;
    int sh_uni_projection;
    int sh_uni_ambient_intensity;
    int sh_uni_light_pos;

    void init_shaders();
    void init_textures();
};

#endif //_GRAPHICSENGINE_H
