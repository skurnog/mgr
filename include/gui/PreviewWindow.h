///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Mar  8 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NONAME_H__
#define __NONAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/sizer.h>
#include <wx/gdicmn.h>
#include <wx/string.h>
#include <wx/frame.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>

///////////////////////////////////////////////////////////////////////////

#include <gui/GLCanvas.h>

///////////////////////////////////////////////////////////////////////////////
/// Class PreviewWindow
///////////////////////////////////////////////////////////////////////////////
class PreviewWindow : public wxFrame
{
private:
    GLCanvas *glCanvas;

protected:

public:

    PreviewWindow( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Preview"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,410 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
    ~PreviewWindow();
};

#endif //__NONAME_H__
