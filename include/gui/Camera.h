#ifndef OSMIORNICASHOOTER_CAMERA_H
#define OSMIORNICASHOOTER_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>

class Camera {
    friend class Scene;
public:
    Camera();
    ~Camera() = default;

    const glm::mat4& getTransformationMatrix() noexcept;
    glm::vec3 getPosition() const noexcept;

    void toogleTranslation(bool translationStatus) { translationEnabled = translationStatus; }
    void translate(glm::vec3 const& vector) noexcept;
    void rotate(double pitch, double yaw) noexcept;

    // spherical
    void rotateAroundOrigin(float, float);
    void zoom(float distanceDelta);
private:
    glm::vec3 position;

    bool translationEnabled = false;

    // Camera rotation
    double pitch = 0;
    double yaw = -90;

    // Camera rotation around point
    float distanceFromOrigin;
    glm::vec2 rotationDegrees;
    const float ROTATION_SPEED = 0.3f;
    const float ROTATION_X_MAX = 85.0f;
    const float ROTATION_X_MIN = -85.0f;
    const float ROTATION_Y_MAX = 175.0f;
    const float ROTATION_Y_MIN = 5.0f;
    const float ZOOM_SPEED = 0.1f;
    const float DISTANCE_MAX = 10.0f;
    const float DISTANCE_MIN = 0.5f;

    // Camera vector space base
    glm::vec3 cameraUp;
    glm::vec3 cameraFront;
    glm::vec3 cameraRight;

    glm::mat4 transformationMatrix;

    void printVec(const glm::vec3 &vector, const std::string& name) const;
    void printVec(const glm::vec2 &vector, const std::string& name) const;

    void updatePosition();
};

#endif //OSMIORNICASHOOTER_CAMERA_H
