// Created by Norbert Skurnóg on 30.03.18.

#ifndef MGR_GLCANVAS_H
#define MGR_GLCANVAS_H

#include <memory>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <epoxy/gl.h>
#include <epoxy/glx.h>

#include <wx/wx.h>
#include <wx/glcanvas.h>
#include <wx/sizer.h>

#include <gui/ShaderProgram.h>
#include <gpu/Cell.h>

class Heightmap;
class Camera;
class GLCanvas : public wxGLCanvas
{
public:
    bool isInitialized = false;

    wxGLContext* m_context;
    std::unique_ptr<ShaderProgram> shader_program;
    std::shared_ptr<Heightmap> heightmap_current;
    std::unique_ptr<Camera> camera;



    std::vector<Cell> result; // for debug purpouses?

    glm::mat4 model_mat;

    int sh_uni_model;
    int sh_uni_view;
    int sh_uni_projection;

    // Transformations to the heightmap model
    glm::vec3 rotation;
    glm::mat4 rotation_mat;
    bool rotation_active = false;
    void rotateModel(double rot_x, double rot_y, double rot_z);
    glm::vec3 translation;
    glm::mat4 translation_mat;
    bool translation_active = false;
    void translateModel(double rot_x, double rot_y, double rot_z);
    float mouse_x_prev = 0;
    float mouse_y_prev = 0;

    glm::mat4 scaling_mat;
    int scale_steps = 0;
    void scaleModel(short step);

    GLuint elements_num;
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;

    /** Preview mesh */
    std::vector<float> mesh_vertices;
    std::vector<unsigned> mesh_indices;
    void generateMesh(unsigned N);
    void* getMeshIndices() { return mesh_indices.data(); }
    unsigned long getMeshIndicesSize() { return mesh_indices.size() * sizeof(unsigned); }
    unsigned long getMeshIndicesCount() { return mesh_indices.size(); }
    void* getMeshVertices() { return mesh_vertices.data(); }
    unsigned long getMeshVerticesSize() { return mesh_vertices.size() * sizeof(float); }

    void init_shaders();
    void init_buffers();
    void setupTextures() const;

    void init();
    GLuint texture_height_id = 0;
    GLuint texture_normal_id = 0;
    GLuint texture_color_id = 0;

    GLint stride = 0;
    bool heightmap_set = false;

    GLCanvas(wxFrame* parent, int* args);
    void setHeightmap(std::shared_ptr<Heightmap> hm);

    virtual ~GLCanvas();

    int getWidth() { return GetSize().x; }
    int getHeight() { return GetSize().y; }

    void nextFrame(wxPaintEvent& evt);
    void prepare3DViewport();

    /** Window events */
    void mouseMoved(wxMouseEvent& event);
    void mouseLeftDown(wxMouseEvent& event);
    void mouseLeftReleased(wxMouseEvent& event);
    void mouseRightDown(wxMouseEvent& event);
    void mouseRightReleased(wxMouseEvent& event);
    void mouseWheelMoved(wxMouseEvent& event);
    void mouseLeftWindow(wxMouseEvent& event);
    void keyPressed(wxKeyEvent& event);
    void keyReleased(wxKeyEvent& event);
    void resized(wxSizeEvent& evt);
    DECLARE_EVENT_TABLE()
};

#endif //MGR_GLCANVAS_H
