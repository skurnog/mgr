#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <string>

#include <epoxy/gl.h>
#include <epoxy/glx.h>

#include <exceptions/OpenGL.h>

#include <Util.h>

class ShaderProgram {
public:
    ShaderProgram() = default;
    ~ShaderProgram() = default;

    void init();
    void load_shader(const std::string& file_path, GLenum shader_type);
    void link() const;
    void use() const;

    inline GLuint get_program_id() { return program_id; }
private:
    unsigned program_id;

    unsigned vertex_sh;
    unsigned fragment_sh;
};

#endif //SHADERPROGRAM_H
