#ifndef MGR_WXIMAGEPANEL_H
#define MGR_WXIMAGEPANEL_H

#include <wx/wx.h>
#include <wx/sizer.h>
#include <paths.h>
#include <resources/image_t.h>
#include <resources/ResourceManager.h>

class wxImagePanel : public wxPanel
{
    wxImage* image;
    wxBitmap resized;
    int w = -1, h = -1;

public:
    explicit wxImagePanel(wxFrame* parent);

    void paintEvent(wxPaintEvent & evt);
    void OnSize(wxSizeEvent& event);
    void render(wxDC& dc);
    void setImage(int width, int height, std::vector<unsigned char> image_bytes);
    void repaint();

    DECLARE_EVENT_TABLE()
};

#endif //MGR_WXIMAGEPANEL_H
