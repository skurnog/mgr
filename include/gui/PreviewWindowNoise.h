///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug 20 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NONAME_H__
#define __NONAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

#include <gui/wxImagePanel.h>
#include <cpu/PerlinNoiseCPU.h>

#include <logger/loggers.h>

///////////////////////////////////////////////////////////////////////////////
/// Class PreviewWindowNoise
///////////////////////////////////////////////////////////////////////////////
class PreviewWindowNoise : public wxFrame
{
private:
    PerlinNoiseCPU perlinNoiseCPU;

protected:
    wxStaticText* m_staticText41;
    wxComboBox* m_comboBox1;
    wxStaticText* m_staticText5;
    wxSpinCtrl* spinCtrl_tex_h;
    wxStaticText* m_staticText4;
    wxSpinCtrl* spinCtrl_tex_w;
    wxStaticText* m_staticText3;
    wxSpinCtrlDouble* spinCtrlDouble_scale;
    wxButton* save_button;
    wxButton* use_button;
    wxCheckBox* checkBox_interactive;
    wxButton* generate_button;
    wxImagePanel* imagePanel;

    std::vector<unsigned char> current_noise;
    unsigned current_noise_w = 0;
    unsigned current_noise_h = 0;
    bool interactive = false;

public:
    PreviewWindowNoise( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Noise preview"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 505,715 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

    ~PreviewWindowNoise();

private:
    void generateNoise();

    void OnSaveButton(wxCommandEvent & event);
    void OnUseButton(wxCommandEvent & event);
    void OnGenerateButton(wxCommandEvent & event);
    void OnHeightSpin(wxCommandEvent & event);
    void OnWidthSpin(wxCommandEvent & event);
    void OnScaleSpin(wxCommandEvent & event);
    void OnAlgorithmCombo(wxCommandEvent & event);
    void OnAutoGenerateCheckbox(wxCommandEvent & event);
};

#endif //__NONAME_H__
