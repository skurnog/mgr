// Created by Norbert Skurnóg on 08.05.18.
#ifndef MGR_CELL_H
#define MGR_CELL_H

struct Cell {
    double terrain_height;
    double water_height;
    double sediment; // suspended sediment amount

    // outflow flux
    double flux_N;
    double flux_S;
    double flux_W;
    double flux_E;

    // velocity vector
    double velocity_x;
    double velocity_y;

    /*** DEBUG */
    bool operator==(const Cell& other) const;
};
typedef struct Cell Cell;

#endif //MGR_CELL_H
