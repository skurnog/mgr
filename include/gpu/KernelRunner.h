#ifndef MGR_KERNEL_RUNNER_H
#define MGR_KERNEL_RUNNER_H


#include <iostream>
#include <memory>
#include <vector>

#include "Util.h"

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MEM_SIZE (128)
#define MAX_SOURCE_SIZE (0x100000)

#include <gpu/Cell.h>

class Heightmap;
class KernelRunner
{
public:
    static std::vector<Cell> runKernel(const std::string&, const Heightmap& inputHeightmap);
    static std::vector<float> runKernelPerlin(int w, int h, double scale);
    static void printCell(const Cell& cell_to_print, int x, int y);

    static void printCellBuffer(Cell* cellBuffer, size_t size_x, size_t size_y);
    static void printDebugOutput(const std::vector<double>& dbgOutput);

//DEBUG
private:
};

#endif