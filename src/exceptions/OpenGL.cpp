#include <exceptions/OpenGL.h>
#include <sstream>
#include <iostream>

using namespace except;

OpenGL::OpenGL(ErrorType error_type, unsigned error_code, const std::string& additional_info)
{
    std::stringstream ss;
    ss << errorDescriptions.at(error_type) << " error " << error_code << ": " << additional_info;
    what_str = ss.str();
    std::cout << what_str << std::endl << std::flush;
}

const char* OpenGL::what()
{
    return what_str.c_str();
}
