//
// Created by norbert on 27.03.18.
//

#include <exceptions/ResourceManager.h>
#include <sstream>
#include <iostream>

using namespace except;

ResourceManager::ResourceManager(unsigned error_code, const string& error_message, const string& additional_info)
{
    std::stringstream ss;
    ss << " error " << error_code << ": " << additional_info << " --- " << error_message;
    what_str = ss.str();
    std::cout << what_str << std::endl; // todo logger
}

const char* ResourceManager::what()
{
    return what_str.c_str();
}
