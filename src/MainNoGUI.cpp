// Created by Norbert Skurnóg on 27.04.18.

#include <paths.h>
#include <gpu/KernelRunner.h>
#include <resources/Heightmap.h>
#include <resources/ResourceManager.h>

#include "cpu/PerlinNoiseCPU.h"

int main()
{
    Util::nextRunNumber();

//    Heightmap hm(RESOURCES_DIR "test_hm_sm2.png");
//    KernelRunner::runKernel(KERNELS_DIR "cell_autom.cl", hm);

    PerlinNoiseCPU perlinNoise;
    std::vector<unsigned char> texture = perlinNoise.noise2D(250,250, 8.89);

    ResourceManager::saveImage(std::string(OUTPUTS_DIR "perlin") + Util::uniqueSuffix(".png"), image_t{texture, 250, 250});
    return 0;
}