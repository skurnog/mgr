#define DEBUG_SYMBOLS_COUNT 10
#define DEBUG_OUT_MEM_SIZE (sizeof(double) * DEBUG_SYMBOLS_COUNT)

/**
 * DOC
 */

#include <gpu/KernelRunner.h>
#include <vector>

#include <resources/Heightmap.h>

#define LOG(RET_VAL) if((RET_VAL) != 0) \
 std::cout<<"ERROR: "<<#RET_VAL<<" RETURNED: "<<(RET_VAL)<<" - "<<Util::oclGetErrorString(RET_VAL)<<std::endl<<std::flush;


void KernelRunner::printCell(const Cell& cell_to_print, int x, int y) {
    std::cout << "Cell (" << x << "," << y << ")" << std::endl;
    std::cout << "terrain:       " << cell_to_print.terrain_height << std::endl;
    std::cout << "water:         " << cell_to_print.water_height << std::endl;
    std::cout << "sediment:      " << cell_to_print.sediment << std::endl;
    std::cout << "flux(N,S,W,E): (" << cell_to_print.flux_N << "," << cell_to_print.flux_S
              << "," << cell_to_print.flux_W << "," << cell_to_print.flux_E << ")" << std::endl;
    std::cout << "velocity(u,v): (" << cell_to_print.velocity_x << "," << cell_to_print.velocity_y << ")" << std::endl;
    std::cout << std::endl << std::flush;
}

std::vector<float> KernelRunner::runKernelPerlin(int w, int h, double scale)
{
    cl_int ret; // to keep the return value of openCL functions

    /* Get Platform and Device Info */
    cl_platform_id platform_id = nullptr;
    cl_uint ret_num_platforms;
    LOG(clGetPlatformIDs(1, &platform_id, &ret_num_platforms));
    cl_device_id device_id = nullptr;
    cl_uint ret_num_devices;
    LOG(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices));
    /* Create OpenCL context */
    cl_context context = clCreateContext(nullptr, 1, &device_id, nullptr, nullptr, &ret);
    /* Create Command Queue */
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
    /* Create Memory Buffer */
//    cl_mem memobj = clCreateBuffer(context, CL_MEM_READ_WRITE, MEM_SIZE * sizeof(char), nullptr, &ret);

    /* Create Kernel Program from the source */
    std::string kernel_source = Util::read_text_file("/home/norbert/z/projekty/CLionProjects/mgr/kernels/perlin.cl");
    size_t source_size = kernel_source.length();
    cl_program program = clCreateProgramWithSource(context, 1, (const char **)&kernel_source, (const size_t *)&source_size, &ret);

    /* Build Kernel Program */

    size_t len;


    LOG(clBuildProgram(program, 1, &device_id, nullptr, nullptr, nullptr));
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, NULL, NULL, &len);
    char *log = new char[len]; //or whatever you use

    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, len, log, nullptr);

    std::cout << log << std::flush;

    /* Create OpenCL Kernel */
    cl_kernel kernel = clCreateKernel(program, "perlin", &ret);

//    /* Set OpenCL Kernel Parameters */
//    LOG(clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobj));
//
//    /* Execute OpenCL Kernel */
//    LOG(clEnqueueTask(command_queue, kernel, 0, nullptr,nullptr));

    std::vector<int> p = {
            151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,190,6,
            148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,
            168,68,175,74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,
            245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,187,208,89,18,169,200,196,135,130,116,188,159,
            86,164,100,109,198,173,186,3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
            47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,153,101,155,167,43,172,9,
            129,22,39,253,19,98,108,110,79,113,224,232,178,185,112,104,218,246,97,228,251,34,242,193,238,210,144,12,
            191,179,162,241,81,51,145,235,249,14,239,107,49,192,214,31,181,199,106,157,184, 84,204,176,115,121,50,
            45,127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180, 151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,190,6,
            148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,
            168,68,175,74,165,71,134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,
            245,40,244,102,143,54,65,25,63,161,1,216,80,73,209,76,132,187,208,89,18,169,200,196,135,130,116,188,159,
            86,164,100,109,198,173,186,3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,
            47,16,58,17,182,189,28,42,223,183,170,213,119,248,152,2,44,154,163,70,221,153,101,155,167,43,172,9,
            129,22,39,253,19,98,108,110,79,113,224,232,178,185,112,104,218,246,97,228,251,34,242,193,238,210,144,12,
            191,179,162,241,81,51,145,235,249,14,239,107,49,192,214,31,181,199,106,157,184, 84,204,176,115,121,50,
            45,127,4,150,254,138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180};



    cl_mem grid_in_mem = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int)*512, p.data(), &ret);
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&grid_in_mem);

    cl_double sc = scale;
    clSetKernelArg(kernel, 1, sizeof(cl_double), &sc);

    cl_mem grid_out_mem = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * w * h, nullptr, &ret);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&grid_out_mem);

    const size_t global_work_size[2] = {(unsigned)w, (unsigned)h};
    clEnqueueNDRangeKernel(command_queue, kernel, 2, nullptr, global_work_size, nullptr, 0, nullptr, nullptr);

    /* Copy results from the memory buffers */
    std::vector<float> return_buffer(w*h);
    clEnqueueReadBuffer(command_queue, grid_out_mem, CL_TRUE, 0, sizeof(float) * w*h, return_buffer.data(), 0, nullptr, nullptr);


//    printCellBuffer(return_buffer.data(), inputHeightmap.getWidth(), inputHeightmap.getHeight());
    /* Finalization */
//    LOG(clFlush(command_queue));
//    ret = clFinish(command_queue);
//    ret = clReleaseKernel(kernel);
//    ret = clReleaseProgram(program);
//    ret = clReleaseMemObject(memobj);
//    ret = clReleaseCommandQueue(command_queue);
//    ret = clReleaseContext(context);

    return return_buffer;
}

std::vector<Cell> KernelRunner::runKernel(const std::string& kernelAbsFilePath, const Heightmap& inputHeightmap)
{
    cl_int ret; // to keep the return value of openCL functions

    /* Get Platform and Device Info */
    cl_platform_id platform_id = nullptr;
    cl_uint ret_num_platforms;
    LOG(clGetPlatformIDs(1, &platform_id, &ret_num_platforms));
    cl_device_id device_id = nullptr;
    cl_uint ret_num_devices;
    LOG(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices));
    /* Create OpenCL context */
    cl_context context = clCreateContext(nullptr, 1, &device_id, nullptr, nullptr, &ret);
    /* Create Command Queue */
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
    /* Create Memory Buffer */
//    cl_mem memobj = clCreateBuffer(context, CL_MEM_READ_WRITE, MEM_SIZE * sizeof(char), nullptr, &ret);

    /* Create Kernel Program from the source */
    std::string kernel_source = Util::read_text_file(kernelAbsFilePath);
    size_t source_size = kernel_source.length();
    cl_program program = clCreateProgramWithSource(context, 1, (const char **)&kernel_source, (const size_t *)&source_size, &ret);

    /* Build Kernel Program */
    LOG(clBuildProgram(program, 1, &device_id, nullptr, nullptr, nullptr));


    size_t len = 0;
    ret = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
    char *buffer = new char[len];
    ret = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, len, buffer, NULL);

    std::cout << buffer << std::endl;

    /* Create OpenCL Kernel */
    cl_kernel kernel = clCreateKernel(program, "cell_update", &ret);
    cl_kernel kernel_step2 = clCreateKernel(program, "step2", &ret);


//    /* Set OpenCL Kernel Parameters */
//    LOG(clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobj));
//
//    /* Execute OpenCL Kernel */
//    LOG(clEnqueueTask(command_queue, kernel, 0, nullptr,nullptr));

    /** Prepare input data */
    std::cout << "cell size in b " << sizeof(Cell) << std::endl;
    std::vector<Cell> cells_vec;
    for(unsigned long i = 0; i < inputHeightmap.getCellsCount(); i++) {
        Cell cell_new{};

        cell_new.terrain_height = (cl_double)inputHeightmap.getHeights().at(i);
        cell_new.water_height   = (cl_double)inputHeightmap.getWaterHeights().at(i);
//        std::cout << cell_new.water_height << std::endl;

        // suspended sediment amount
        cell_new.sediment       = 0;

        // outflow flux
        cell_new.flux_N         = 0;
        cell_new.flux_S         = 0;
        cell_new.flux_W         = 0;
        cell_new.flux_E         = 0;

        // velocity vector
        cell_new.velocity_x     = 0;
        cell_new.velocity_y     = 0;

        cells_vec.push_back(cell_new);
    }

    cl_mem grid_in_mem = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Cell) * inputHeightmap.getCellsCount(), cells_vec.data(), &ret);
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&grid_in_mem);

    cl_mem grid_out_mem = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(Cell) * inputHeightmap.getCellsCount(), nullptr, &ret);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&grid_out_mem);

    cl_mem debug_out_mem = clCreateBuffer(context, CL_MEM_WRITE_ONLY, DEBUG_OUT_MEM_SIZE, nullptr, &ret);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&debug_out_mem);

    const size_t global_work_size[2] = {inputHeightmap.getWidth(), inputHeightmap.getHeight()};
    clEnqueueNDRangeKernel(command_queue, kernel, 2, nullptr, global_work_size, nullptr, 0, nullptr, nullptr);

    /* Copy results from the memory buffers */
    std::vector<Cell> return_buffer(inputHeightmap.getCellsCount());
    clEnqueueReadBuffer(command_queue, grid_out_mem, CL_TRUE, 0, sizeof(Cell) * inputHeightmap.getCellsCount(), return_buffer.data(), 0, nullptr, nullptr);

    std::vector<double> debug_output(DEBUG_SYMBOLS_COUNT);
    clEnqueueReadBuffer(command_queue, debug_out_mem, CL_TRUE, 0, DEBUG_OUT_MEM_SIZE, debug_output.data(), 0, nullptr, nullptr);

//    printCellBuffer(return_buffer.data(), inputHeightmap.getWidth(), inputHeightmap.getHeight());
    printDebugOutput(debug_output);




    // STEP2

    cl_mem grid_in_mem2 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Cell) * inputHeightmap.getCellsCount(), return_buffer.data(), &ret);
    clSetKernelArg(kernel_step2, 0, sizeof(cl_mem), (void *)&grid_in_mem2);

    cl_mem grid_out_mem2 = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(Cell) * inputHeightmap.getCellsCount(), nullptr, &ret);
    clSetKernelArg(kernel_step2, 1, sizeof(cl_mem), (void *)&grid_out_mem2);

    cl_mem debug_out_mem2 = clCreateBuffer(context, CL_MEM_WRITE_ONLY, DEBUG_OUT_MEM_SIZE, nullptr, &ret);
    clSetKernelArg(kernel_step2, 2, sizeof(cl_mem), (void *)&debug_out_mem2);

//    const size_t global_work_size2[2] = {inputHeightmap.getWidth(), inputHeightmap.getHeight()};
    clEnqueueNDRangeKernel(command_queue, kernel_step2, 2, nullptr, global_work_size, nullptr, 0, nullptr, nullptr);

    /* Copy results from the memory buffers */
    std::vector<Cell> return_buffer2(inputHeightmap.getCellsCount());
    clEnqueueReadBuffer(command_queue, grid_out_mem2, CL_TRUE, 0, sizeof(Cell) * inputHeightmap.getCellsCount(), return_buffer2.data(), 0, nullptr, nullptr);

    std::vector<double> debug_output2(DEBUG_SYMBOLS_COUNT);
    clEnqueueReadBuffer(command_queue, debug_out_mem2, CL_TRUE, 0, DEBUG_OUT_MEM_SIZE, debug_output2.data(), 0, nullptr, nullptr);


    /* Finalization */
//    LOG(clFlush(command_queue));
//    ret = clFinish(command_queue);
//    ret = clReleaseKernel(kernel);
//    ret = clReleaseProgram(program);
//    ret = clReleaseMemObject(memobj);
//    ret = clReleaseCommandQueue(command_queue);
//    ret = clReleaseContext(context);

    return return_buffer2;
}

void KernelRunner::printCellBuffer(Cell* cellBuffer, size_t size_x, size_t size_y) {/* Display Result */
    for(int i = 0; i < size_x; i++) {
        for(int j = 0; j < size_y; j++) printCell(Cell(cellBuffer[i * size_x + j]), i, j);
    }
    std::cout << std::flush;
}

void KernelRunner::printDebugOutput(const std::vector<double>& dbgOutput) {
    std::cout << std::endl;
    for(int i = 0; i < dbgOutput.size(); i++) {
        std::cout << "Kernel debug " << i << ": " << dbgOutput.at(i) << std::endl;
    }
    std::cout << std::endl << std::flush;
}

#undef LOG