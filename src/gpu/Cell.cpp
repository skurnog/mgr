// Created by Norbert Skurnóg on 20.05.18.
#include <gpu/Cell.h>

#include <typeinfo>

bool Cell::operator==(const Cell& other) const
{
    if (typeid(*this) != typeid(other))
        return false;

    return terrain_height == other.terrain_height;
}
