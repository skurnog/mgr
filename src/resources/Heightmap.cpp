// Created by Norbert Skurnóg on 21.03.18.
#include <resources/Heightmap.h>

#include <iostream>
#include <resources/ResourceManager.h>
#include <gpu/Cell.h>
#include <paths.h>

/**
 * normalmapa/colormapa muszą mieć identyczne rozmiary (póki co?)
 * @param heightmap_path
 * @param normalmap_path
 * @param colormap_path
 */
Heightmap::Heightmap(const std::string& heightmap_path, const std::string& normalmap_path, const std::string& colormap_path)
{
    image_t hm = ResourceManager::loadImage(heightmap_path);
    this->heightmap_bytes = std::move(hm.image_bytes);
    this->width = hm.width;
    this->height = hm.height;
    heighmap_to_heights();

    if(!normalmap_path.empty()) {
        image_t nm = ResourceManager::loadImage(normalmap_path);
        this->normalmap_bytes = std::move(nm.image_bytes);
    }
    if(!colormap_path.empty()) {
        image_t cm = ResourceManager::loadImage(colormap_path);
        this->colormap_bytes = std::move(cm.image_bytes);
    }
}

void Heightmap::setHeights(const std::vector<Cell> &cell_vec) {
    std::vector<unsigned char> newHeightmap;
    std::vector<unsigned char> newWatermap;
    for(Cell element: cell_vec) {
        newHeightmap.push_back((unsigned char) element.terrain_height);
        newHeightmap.push_back((unsigned char) element.terrain_height);
        newHeightmap.push_back((unsigned char) element.terrain_height);
        newHeightmap.push_back(255);

        newWatermap.push_back((unsigned char) element.water_height);
        newWatermap.push_back((unsigned char) element.water_height);
        newWatermap.push_back((unsigned char) element.water_height);
        newWatermap.push_back(255);
    }
//    std::vector<unsigned char> oldhm = this->heightmap_bytes;
    this->heightmap_bytes = std::move(newHeightmap);
    heighmap_to_heights();
    this->watermap_bytes = std::move(newWatermap);
    watermap_to_water_heights();
}

void Heightmap::loadWatermap(const std::string& filePath)
{
    image_t hm = ResourceManager::loadImage(filePath);
    this->watermap_bytes = std::move(hm.image_bytes);
    watermap_to_water_heights();
}

size_t Heightmap::getWidth() const
{
    return width;
}

size_t Heightmap::getHeight() const
{
    return height;
}

size_t Heightmap::getCellsCount() const
{
    return width * height;
}

/** Get useful data from loaded image. WE ASSUME RGBA
 * R - terrain height
 * B - unused
 * G - unused
 * A - unused
 */
void Heightmap::heighmap_to_heights()
{
    for(size_t i = 0; i < heightmap_bytes.size(); i+=4) {
        auto height = heightmap_bytes.at(i); // values in range of 0 - 255
        heights.push_back(height);
    }
}

/** Get useful data from loaded image. WE ASSUME RGBA */
void Heightmap::watermap_to_water_heights()
{
    for(size_t i = 0; i < watermap_bytes.size(); i+=4) {
        auto height = watermap_bytes.at(i); // values in range of 0 - 255
        water_heights.push_back(height);
//        std::cout << (int)height << std:: endl;
    }
}
