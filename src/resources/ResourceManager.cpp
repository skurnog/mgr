//
// Created by norbert on 21.03.18.
//

#include <resources/ResourceManager.h>
#include <resources/Heightmap.h>
#include <exceptions/ResourceManager.h>

#include <iostream>

#include <lodepng.h>

image_t ResourceManager::loadImage(const string &file_path) {
    std::vector<unsigned char> image_bytes; //the raw pixels
    unsigned width, height;

    //decode
    unsigned error = lodepng::decode(image_bytes, width, height, file_path);
    if(error) {
        throw except::ResourceManager(error, string("lodepng: ") + string(lodepng_error_text(error)), file_path);
    }

    return {std::move(image_bytes), width, height};
    //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
//    return image;
}

void ResourceManager::saveImage(const ResourceManager::string &file_path, const image_t &image) {
    unsigned error = lodepng::encode(file_path, image.image_bytes, image.width, image.height);

    //if there's an error, display it
    if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
}


