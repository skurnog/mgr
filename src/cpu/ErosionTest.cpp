// Created by Norbert Skurnóg on 20.05.18.
#include <cpu/ErosionTest.h>

#include <gpu/Cell.h>
#include <resources/Heightmap.h>

#include <cmath>
#include <iostream>

std::vector<Cell> ErosionTest::run(const Heightmap &inputHeightmap) {
    grid_w = inputHeightmap.getWidth();
    grid_h = inputHeightmap.getHeight();
    x_max = grid_w - 1;
    y_max = grid_h - 1;

    /** Prepare input data */
    std::vector<Cell> input_grid;
    for(unsigned long i = 0; i < inputHeightmap.getCellsCount(); i++) {
        Cell cell_new{};

        cell_new.terrain_height = inputHeightmap.getHeights().at(i);
        cell_new.water_height   = inputHeightmap.getWaterHeights().at(i);

        // suspended sediment amount
        cell_new.sediment       = 0;

        // outflow flux
        cell_new.flux_N         = 0;
        cell_new.flux_S         = 0;
        cell_new.flux_W         = 0;
        cell_new.flux_E         = 0;

        // velocity vector
        cell_new.velocity_x     = 0;
        cell_new.velocity_y     = 0;

        input_grid.push_back(cell_new);
    }
    std::vector<Cell> output_grid(inputHeightmap.getCellsCount());
    std::vector<Cell> output_grid2(inputHeightmap.getCellsCount());

    step1(input_grid.data(), output_grid.data());
    run_step(output_grid.data(), output_grid2.data());

    return output_grid2;
}

void ErosionTest::run_step(Cell* input_grid, Cell* output_grid)
{
    // stałe póki co tutaj
    double delta_N = 1;
    double pipe_area_C = 1;
    double pipe_Wength_C = 1;
    double gravity_const_C = 9.8;
    double dist_X = 1, dist_Y = 1; // odległość między kolejnymi punktami na siatce, im mniejsza tym większa rozdzielczość symulacji
    double sediment_capacity_C = 10;
    double sediment_dissolving_C = 1;
    // stałe

    Cell cell_default{};
    cell_default.sediment = 0;
    cell_default.terrain_height = 0;
    cell_default.water_height = 0;
    cell_default.sediment = 0;
    cell_default.flux_N = 0;
    cell_default.flux_S = 0;
    cell_default.flux_W = 0;
    cell_default.flux_E = 0;
    cell_default.velocity_x = 0;
    cell_default.velocity_y = 0;

    for(int x = 0; x < grid_w; x++) {
        for(int y = 0; y < grid_h; y++) {

            Cell cell_current = input_grid[x + y * grid_w];
            Cell cell_N = cell_default;
            Cell cell_S = cell_default;
            Cell cell_W = cell_default;
            Cell cell_E = cell_default;
            double flux_N_updated = 0;
            double flux_S_updated = 0;
            double flux_W_updated = 0;
            double flux_E_updated = 0;


            // output flux values have to be positive since gravity doesn't work upside down
            // output flux has to be 0 at the border to avoid water leaking over the grid
            // output flux scaling factor has to be used to avoid negative water level after update
            if(y - 1 >= 0){
                cell_N = input_grid[x + (y - 1) * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_N.terrain_height - cell_N.water_height;
                flux_N_updated = fmax(cell_current.flux_N + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0);
            }
            if(y + 1 <= y_max){
                cell_S = input_grid[x + (y + 1) * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_S.terrain_height - cell_S.water_height;
                flux_S_updated = fmax(cell_current.flux_S + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0);
            }
            if(x - 1 >= 0){
                cell_W = input_grid[(x - 1) + y * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_W.terrain_height - cell_W.water_height;
                flux_W_updated = fmax(cell_current.flux_W + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0);
            }
            if(x + 1 <= x_max){
                cell_E = input_grid[(x + 1) + y * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_E.terrain_height - cell_E.water_height;
                flux_E_updated = fmax(cell_current.flux_E + delta_N * pipe_area_C * (gravity_const_C * delta_h/pipe_Wength_C), 0);
            }

            // update outflow flux values
            double flux_out_sum = flux_N_updated + flux_S_updated + flux_W_updated + flux_E_updated;
            double flux_scaling_factor = 1;
            // if(flux_out_sum > 0){
            //     flux_scaling_factor = cell_current.water_height * dist_X * dist_Y/flux_out_sum;
            //     if(flux_scaling_factor > 1) flux_scaling_factor = 1; // fmin nie działa z jakiegoś powodu...
            // }

            flux_N_updated = flux_N_updated * flux_scaling_factor;
            flux_S_updated = flux_S_updated * flux_scaling_factor;
            flux_W_updated = flux_W_updated * flux_scaling_factor;
            flux_E_updated = flux_E_updated * flux_scaling_factor;

            // update water basing on out flux and in flux
            double flux_in_sum = cell_N.flux_S + cell_S.flux_N + cell_W.flux_E + cell_E.flux_W;
            double water_height_delta = flux_in_sum - flux_out_sum * flux_scaling_factor; // 1 - 1 = 5.96046e-08
            double water_height_updated = cell_current.water_height + water_height_delta/(dist_X * dist_Y); // divided by lx * ly (distance between grid points)

            // calculate velocity vector
            // symulacja jest kilkukrokowa, pozniej trzeba to sensownie rozdzielic
            double water_height_d1_d2_avg = (water_height_updated + cell_current.water_height)/2;

            double water_x_delta = (cell_W.flux_E - cell_current.flux_W + cell_current.flux_E - cell_E.flux_W)/2;
            double velocity_x = water_x_delta/(water_height_d1_d2_avg * dist_Y);

            double water_y_delta = (cell_N.flux_S - cell_current.flux_N + cell_current.flux_S - cell_S.flux_N)/2;
            double velocity_y = water_y_delta/(water_height_d1_d2_avg * dist_X);

            // erosion and deposition
            // calculate tilt angle
            double local_tilt_angle = 0;
            int x_plus = (x == x_max) ? x : x + 1;
            int y_plus = (y == y_max) ? y : y + 1;
            int x_minus = (x == 0) ? x : x - 1;
            int y_minus = (y == 0) ? y : y - 1;

            double dhdx = (input_grid[x_plus + y * grid_w].terrain_height - input_grid[x_minus + y * grid_w].terrain_height) / (2);
            double dhdy = (input_grid[x + y_plus * grid_w].terrain_height - input_grid[x + y_minus * grid_w].terrain_height) / (2);
            double sin_alfa = (sqrt(dhdx*dhdx + dhdy*dhdy))/(sqrt(1 + dhdx*dhdx + dhdy*dhdy));
            std::cout << "sinalfa( " << x << "," << y << "): " << sin_alfa << std::endl;//std::flush;

            double velocity_val = sqrt(velocity_y*velocity_y + velocity_x*velocity_x);
            double sediment_transport_capacity = sediment_capacity_C * sin_alfa * velocity_val;

            double terrain_height_updated = 0;
            double sediment_updated = 0;
            // decide if deposit or erode
            if(sediment_transport_capacity > cell_current.sediment) {
                terrain_height_updated = cell_current.terrain_height - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
                sediment_updated = cell_current.sediment + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
            } else {
                //deposit sediment
                terrain_height_updated = cell_current.terrain_height + sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
                sediment_updated = cell_current.sediment - sediment_dissolving_C * (sediment_transport_capacity - cell_current.sediment);
            }

            // sediment transport

            Cell cell_updated{};
            if(terrain_height_updated < 0) terrain_height_updated = 0; // tymczasowo, proces depozycji powinien to ogarnac
            cell_updated.terrain_height = terrain_height_updated;
            cell_updated.water_height = water_height_updated;
            cell_updated.sediment = sediment_updated;
            cell_updated.flux_N = flux_N_updated * flux_scaling_factor;
            cell_updated.flux_S = flux_S_updated * flux_scaling_factor;
            cell_updated.flux_W = flux_W_updated * flux_scaling_factor;
            cell_updated.flux_E = flux_E_updated * flux_scaling_factor;
            cell_updated.velocity_x = velocity_x;
            cell_updated.velocity_y = velocity_y;

            output_grid[x + y * grid_w] = cell_updated;
        }
    }
}

void ErosionTest::step1(Cell* input_grid, Cell* output_grid)
{
    // stałe póki co tutaj
    double delta_N = 1;
    double pipe_area_C = 1;
    double pipe_Wength_C = 1;
    double gravity_const_C = 9.8;
    double dist_X = 1, dist_Y = 1; // odległość między kolejnymi punktami na siatce, im mniejsza tym większa rozdzielczość symulacji
    double sediment_capacity_C = 10;
    double sediment_dissolving_C = 1;
    // stałe

    Cell cell_default{};
    cell_default.sediment = 0;
    cell_default.terrain_height = 0;
    cell_default.water_height = 0;
    cell_default.sediment = 0;
    cell_default.flux_N = 0;
    cell_default.flux_S = 0;
    cell_default.flux_W = 0;
    cell_default.flux_E = 0;
    cell_default.velocity_x = 0;
    cell_default.velocity_y = 0;

    for(int x = 0; x < grid_w; x++) {
        for (int y = 0; y < grid_h; y++) {

            Cell cell_current = input_grid[x + y * grid_w];
            Cell cell_N = cell_default;
            Cell cell_S = cell_default;
            Cell cell_W = cell_default;
            Cell cell_E = cell_default;
            double flux_N_updated = 0;
            double flux_S_updated = 0;
            double flux_W_updated = 0;
            double flux_E_updated = 0;


            // output flux values have to be positive since gravity doesn't work upside down
            // output flux has to be 0 at the border to avoid water leaking over the grid
            // output flux scaling factor has to be used to avoid negative water level after update
            if (y - 1 >= 0) {
                cell_N = input_grid[x + (y - 1) * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_N.terrain_height -
                                 cell_N.water_height;
                flux_N_updated = fmax(
                        cell_current.flux_N + delta_N * pipe_area_C * (gravity_const_C * delta_h / pipe_Wength_C), 0);
            }
            if (y + 1 <= y_max) {
                cell_S = input_grid[x + (y + 1) * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_S.terrain_height -
                                 cell_S.water_height;
                flux_S_updated = fmax(
                        cell_current.flux_S + delta_N * pipe_area_C * (gravity_const_C * delta_h / pipe_Wength_C), 0);
            }
            if (x - 1 >= 0) {
                cell_W = input_grid[(x - 1) + y * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_W.terrain_height -
                                 cell_W.water_height;
                flux_W_updated = fmax(
                        cell_current.flux_W + delta_N * pipe_area_C * (gravity_const_C * delta_h / pipe_Wength_C), 0);
            }
            if (x + 1 <= x_max) {
                cell_E = input_grid[(x + 1) + y * grid_w];

                double delta_h = cell_current.terrain_height + cell_current.water_height - cell_E.terrain_height -
                                 cell_E.water_height;
                flux_E_updated = fmax(
                        cell_current.flux_E + delta_N * pipe_area_C * (gravity_const_C * delta_h / pipe_Wength_C), 0);
            }

            // update outflow flux values
            double flux_out_sum = flux_N_updated + flux_S_updated + flux_W_updated + flux_E_updated;
            double flux_scaling_factor = 1;
            // if(flux_out_sum > 0){
            //     flux_scaling_factor = cell_current.water_height * dist_X * dist_Y/flux_out_sum;
            //     if(flux_scaling_factor > 1) flux_scaling_factor = 1; // fmin nie działa z jakiegoś powodu...
            // }

            flux_N_updated = flux_N_updated * flux_scaling_factor;
            flux_S_updated = flux_S_updated * flux_scaling_factor;
            flux_W_updated = flux_W_updated * flux_scaling_factor;
            flux_E_updated = flux_E_updated * flux_scaling_factor;

            Cell cell_updated{};
            cell_updated.terrain_height = cell_current.terrain_height;
            cell_updated.water_height = cell_current.water_height;
            cell_updated.sediment = cell_current.sediment;
            cell_updated.flux_N = flux_N_updated;
            cell_updated.flux_S = flux_S_updated;
            cell_updated.flux_W = flux_W_updated;
            cell_updated.flux_E = flux_E_updated;
            cell_updated.velocity_x = cell_current.velocity_x;
            cell_updated.velocity_y = cell_current.velocity_y;

            output_grid[x + y * grid_w] = cell_updated;
        }
    }
}

size_t ErosionTest::grid_w = 0;
size_t ErosionTest::grid_h = 0;
size_t ErosionTest::x_max = 0;
size_t ErosionTest::y_max = 0;
