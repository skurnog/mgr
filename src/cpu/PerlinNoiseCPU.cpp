#include <cpu/PerlinNoiseCPU.h>
#include <iostream>

PerlinNoiseCPU::PerlinNoiseCPU()
{
    for(int i=0; i<512; i++) p.push_back(permutation_base[i%256]);
}

double PerlinNoiseCPU::noise(double x, double y, double z) {
    // Find the unit cube that the point is located in.
    int X = int(x) & 255;
    int Y = int(y) & 255;
    int Z = int(z) & 255;

    // Find point location relatively to the cube.
    double xd = x - X;
    double yd = y - Y;
    double zd = z - Z;

    // Compute fade curves.
    double u = fade(xd);
    double v = fade(yd);
    double w = fade(zd);

    int A = p[X] + Y;
    int AA = p[A] + Z;
    int AB = p[A + 1] + Z;

    int B = p[X + 1] + Y;
    int BA = p[B] + Z;
    int BB = p[B + 1] + Z;

    double retval = lerp(w, lerp(v, lerp(u, grad(p[AA], xd, yd, zd),
                                   grad(p[BA], xd - 1, yd, zd)),
                            lerp(u, grad(p[AB], xd, yd - 1, zd),
                                   grad(p[BB], xd - 1, yd - 1, zd))),
                    lerp(v, lerp(u, grad(p[AA + 1], xd, yd, zd - 1),
                                   grad(p[BA + 1], xd - 1, yd, zd - 1)),
                            lerp(u, grad(p[AB + 1], xd, y - 1, zd - 1),
                                   grad(p[BB + 1], xd - 1, yd - 1, zd - 1))));
    return (retval+1)/2; // Desirable values are in range [0-1]
}

std::vector<unsigned char> PerlinNoiseCPU::noise2D(int w, int h, double scale)
{
    std::vector<unsigned char> texture;
    for(int i=0; i<w; i++) {
        for(int j=0; j<h; j++) {
            double noise_val = this->noise((i/(double)w)*scale, (j/(double)h)*scale, 0);
            auto map_val = (unsigned char)(noise_val * 255);
            texture.push_back(map_val);
            texture.push_back(map_val);
            texture.push_back(map_val);
            texture.push_back(255);
        }
    }
    return texture;
}

/***
 * Function used to smooth the values.
 * @param t
 * @return
 */
double PerlinNoiseCPU::fade(double t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

/***
 * Linear interpolation
 * @param t
 * @param a
 * @param b
 * @return
 */
double PerlinNoiseCPU::lerp(double t, double a, double b)
{
    return a + t * (b - a);
}

/***
 * Calculate the dot product of a randomly selected gradient vector and the 8 location vectors.
 * Implementation by Ken Perlin.
 * @param hash
 * @param x
 * @param y
 * @param z
 * @return
 */
double PerlinNoiseCPU::grad(int hash, double x, double y, double z)
{
    int h = hash & 15;
    double u = h<8 ? x : y;
    double v = h<4 ? y : h==12||h==14 ? x : z;
    return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}