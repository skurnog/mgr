// Created by Norbert Skurnóg on 30.03.18.
#include <gui/GLCanvas.h>

#include <paths.h>
#include <gui/Camera.h>
#include <logger/loggers.h>
#include <resources/ResourceManager.h>
#include <resources/Heightmap.h>

#include <glm/gtc/matrix_transform.hpp>
#include <gpu/KernelRunner.h>
#include <cpu/ErosionTest.h>

#include "EXR.h"

#define ROTATION_SPEED 0.3f
#define TRANSLATION_SPEED 0.001f
#define SCALING_SPEED 0.05f

BEGIN_EVENT_TABLE(GLCanvas, wxGLCanvas)
        EVT_MOTION(GLCanvas::mouseMoved)
        EVT_LEFT_DOWN(GLCanvas::mouseLeftDown)
        EVT_LEFT_UP(GLCanvas::mouseLeftReleased)
        EVT_RIGHT_DOWN(GLCanvas::mouseRightDown)
        EVT_RIGHT_UP(GLCanvas::mouseRightReleased)
        EVT_LEAVE_WINDOW(GLCanvas::mouseLeftWindow)
        EVT_SIZE(GLCanvas::resized)
        EVT_KEY_DOWN(GLCanvas::keyPressed)
        EVT_KEY_UP(GLCanvas::keyReleased)
        EVT_MOUSEWHEEL(GLCanvas::mouseWheelMoved)
        EVT_PAINT(GLCanvas::nextFrame)
END_EVENT_TABLE()

GLCanvas::GLCanvas(wxFrame* parent, int* args) :
        wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
{
    m_context = new wxGLContext(this);
    shader_program = std::make_unique<ShaderProgram>();
    camera = std::make_unique<Camera>();
    generateMesh(1000);

    model_mat = glm::mat4(1.0f);
    rotation_mat = glm::mat4(1.0f);
    translation_mat = glm::mat4(1.0f);
    translation = glm::vec3(0.0f);
    scaling_mat = glm::mat4(1.0f);

    SetBackgroundStyle(wxBG_STYLE_CUSTOM); // To avoid flashing on MSW
}

void GLCanvas::setHeightmap(std::shared_ptr<Heightmap> hm) {
    heightmap_set = true;

    heightmap_current = std::move(hm);
    setupTextures();

}

void GLCanvas::setupTextures() const {
    size_t width = heightmap_current->getWidth();
    size_t height = heightmap_current->getHeight();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_height_id);
    glTexImage2D(GL_TEXTURE_2D, 0, static_cast<int>(GL_RGBA), width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, heightmap_current->getHeightmapTexture().data());

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture_normal_id);
    glTexImage2D(GL_TEXTURE_2D, 0, static_cast<int>(GL_RGBA), width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, heightmap_current->getNormalmapTexture().data());

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture_color_id);
    glTexImage2D(GL_TEXTURE_2D, 0, static_cast<int>(GL_RGBA), width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, heightmap_current->getColorTexture().data());
}

GLCanvas::~GLCanvas()
{
    delete m_context;
}

// EVENTS
void GLCanvas::mouseMoved(wxMouseEvent& event)
{
    float mouse_x_curr = event.GetPosition().x;
    float mouse_y_curr = event.GetPosition().y;
    float rot_x = (mouse_y_curr - mouse_y_prev);
    float rot_y = (mouse_x_curr - mouse_x_prev);
    mouse_x_prev = mouse_x_curr;
    mouse_y_prev = mouse_y_curr;

    if(rotation_active) {
        rotateModel(rot_x, rot_y, 0.0f);
//    camera->rotateAroundOrigin(rot_y, rot_x);
    }
    if(translation_active) {
        translateModel(rot_x, rot_y, 0.0f);
    }
//    camera->translate(glm::vec3(rot_x, 0, rot_y));
    Refresh();
}
void GLCanvas::mouseLeftDown(wxMouseEvent& event)
{
    mouse_x_prev = event.GetPosition().x;
    mouse_y_prev = event.GetPosition().y;
    rotation_active = true;
}
void GLCanvas::mouseLeftReleased(wxMouseEvent& event)
{
    rotation_active = false;
}
void GLCanvas::mouseRightDown(wxMouseEvent& event)
{
    mouse_x_prev = event.GetPosition().x;
    mouse_y_prev = event.GetPosition().y;
    translation_active = true;
//    camera->toogleTranslation(true);
}
void GLCanvas::mouseRightReleased(wxMouseEvent& event)
{
    translation_active = false;
//    camera->toogleTranslation(false);
}
void GLCanvas::mouseWheelMoved(wxMouseEvent& event)
{
    camera->zoom(event.m_wheelRotation > 0 ? (short)-1 : (short)1);

    Refresh();
}
void GLCanvas::mouseLeftWindow(wxMouseEvent& event) {}
void GLCanvas::keyPressed(wxKeyEvent& event) {
    if(event.m_keyCode == WXK_UP) {
        camera->rotateAroundOrigin(0,1);
    } else if(event.m_keyCode == WXK_DOWN){
        camera->rotateAroundOrigin(0,-1);
    } else if(event.m_keyCode == WXK_LEFT){
        camera->rotateAroundOrigin(-1,0);
    } else if(event.m_keyCode == WXK_RIGHT){
        camera->rotateAroundOrigin(1,0);
    }

    std::cout << event.GetUnicodeKey() << "-" << (int)'l' << std::flush;

    // 'l' - LOAD
    if(event.GetKeyCode() == 76) {
        auto hm = std::make_shared<Heightmap>(
                RESOURCES_DIR "stozek_hm.png",
                RESOURCES_DIR "stozek_norm.png",
                RESOURCES_DIR "stozek_color.png");
        hm->loadWatermap(RESOURCES_DIR "stozek_wm3.png");
        setHeightmap(hm);
    }
    // - 'k' - LOAD ISLAND
    if(event.GetKeyCode() == 75) {
        auto hm = std::make_shared<Heightmap>(
                RESOURCES_DIR "island_sm.png",
                RESOURCES_DIR "stozek_norm.png",
                RESOURCES_DIR "stozek_color.png");
        hm->loadWatermap(RESOURCES_DIR "stozek_wm3.png");
        setHeightmap(hm);
    }
    // 's' - SAVE
    if(event.m_keyCode == 83) {
        ResourceManager::saveImage(std::string(OUTPUTS_DIR "hmap") + Util::uniqueSuffix(".png"),
                                   image_t{heightmap_current->getHeightmapTexture(), heightmap_current->getWidth(), heightmap_current->getHeight()});
    }
    // 'r' - RUN
    if(event.m_keyCode == 82) {
//        Util::dump_bitmap_vec(heightmap_current->getHeights(), "old");
        result = KernelRunner::runKernel(KERNELS_DIR "cell_autom.cl", *heightmap_current);
        heightmap_current->setHeights(result);
//        Util::dump_bitmap_vec(heightmap_current->getHeights(), "new");
        setupTextures();
    }
    // 't' - RUN CPU
    if(event.m_keyCode == 84) {
        result = ErosionTest::run(*heightmap_current);
        heightmap_current->setHeights(result);
        setupTextures();
    }
    // 'd' - save debug bitmap
    if(event.m_keyCode == 68) {
        // prepare debug data
        std::string outfn = std::string(OUTPUTS_DIR "dbgmap") + Util::uniqueSuffix(".json");
        std::ofstream outJSON(outfn);
        outJSON << "{ \"width\":" << heightmap_current->getWidth() << "," << "\"height\": " << heightmap_current->getHeight() << ",\"data\":[";

        std::vector<float> debug_data;
        for(Cell element: result) {
            outJSON << "[";
            outJSON << element.flux_N << "," << element.flux_S << "," << element.flux_W << "," << element.flux_E;
//            outJSON << element.water_height;
            outJSON << "],";
        }
        outJSON << "]}";
        outJSON.flush();

    }
    Refresh();
}


void GLCanvas::keyReleased(wxKeyEvent& event) {}
void GLCanvas::resized(wxSizeEvent& evt)
{
//	wxGLCanvas::OnSize(evt);

    Refresh();
}

// initialization
void GLCanvas::init()
{
    if(!isInitialized) {
        try {
            wxGLCanvas::SetCurrent(*m_context);
            glGenTextures(1, &texture_height_id);
            glGenTextures(1, &texture_normal_id);
            glGenTextures(1, &texture_color_id);

            init_shaders();
            init_buffers();
            isInitialized = true;
        } catch (except::OpenGL& ex) {
            std::cout << ex.what();
            throw;
        }
    }
}

void GLCanvas::init_shaders() {
    shader_program->init();
    shader_program->load_shader(SHADERS_DIR "displacement.frag", GL_FRAGMENT_SHADER);  // todo exceptions
    shader_program->load_shader(SHADERS_DIR "displacement.vert", GL_VERTEX_SHADER);
    shader_program->link();
    shader_program->use();

    // setup uniforms
    sh_uni_model = glGetUniformLocation(shader_program->get_program_id(), "modelMat");
    sh_uni_view = glGetUniformLocation(shader_program->get_program_id(), "viewMat");
    sh_uni_projection = glGetUniformLocation(shader_program->get_program_id(), "projectionMat");

    glUniform1i(glGetUniformLocation(shader_program->get_program_id(), "textureDepth"), 0);
    glUniform1i(glGetUniformLocation(shader_program->get_program_id(), "textureNormal"), 1);
    glUniform1i(glGetUniformLocation(shader_program->get_program_id(), "textureColor"), 2);
}

void GLCanvas::generateMesh(unsigned N) {
    mesh_indices.clear();
    mesh_vertices.clear();

    for (int j=0; j<=N; ++j)
    {
        for (int i=0; i<=N; ++i)
        {
            float x = (float)i/(float)N;
            float y = (float)j/(float)N;
            float z = 0;
            mesh_vertices.push_back(x);
            mesh_vertices.push_back(y);
            mesh_vertices.push_back(z);

            mesh_vertices.push_back(x);
            mesh_vertices.push_back(y);
        }
    }

    for (int j=0; j<N; ++j)
    {
        for (int i=0; i<N; ++i)
        {
            int row1 = j * (N+1);
            int row2 = (j+1) * (N+1);

            // triangle 1
            mesh_indices.push_back(row1+i);
            mesh_indices.push_back(row1+i+1);
            mesh_indices.push_back(row2+i+1);

            // triangle 2
            mesh_indices.push_back(row1+i);
            mesh_indices.push_back(row2+i+1);
            mesh_indices.push_back(row2+i);
        }
    }
}

void GLCanvas::init_buffers() {
    stride = (3+2) * sizeof(GLfloat);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, getMeshVerticesSize(), getMeshVertices(), GL_STATIC_DRAW);

    // specify vertex attributes // TODO exceptions/some kind of error checking at least
    GLint attr = glGetAttribLocation(shader_program->get_program_id(), "position");
    glVertexAttribPointer(attr, 3, GL_FLOAT, GL_FALSE, stride, (void *) 0);
    glEnableVertexAttribArray(attr);

    attr = glGetAttribLocation(shader_program->get_program_id(), "TexCoord");
    glVertexAttribPointer(attr, 2, GL_FLOAT, GL_FALSE, stride, (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(attr);

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, getMeshIndicesSize(), getMeshIndices(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

// Drawing
/** Inits the OpenGL viewport for drawing in 3D. */
void GLCanvas::prepare3DViewport()
{
    if(!IsShown()) return;
    try {
        init();
    } catch (except::OpenGL& ex) {
        std::cout << ex.what();
    }

    wxPaintDC(this); // only to be used in paint events. use wxClientDC to paint outside the paint event

    glClearColor(0.0f, 0.0f, 0.5f, 1.0f); // Black Background
    glViewport(0, 0, getWidth(), getHeight());
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
}

void GLCanvas::rotateModel(double rot_x, double rot_y, double rot_z)
{
    rotation += glm::radians(glm::vec3(rot_x, rot_y, rot_z) * ROTATION_SPEED);
    rotation.x = rotation.x >= 360 ? rotation.x - 360 : rotation.x;
    rotation.y = rotation.y >= 360 ? rotation.y - 360 : rotation.y;
    rotation.z = rotation.z >= 360 ? rotation.z - 360 : rotation.z;
    rotation_mat = glm::mat4(1.0f);
    rotation_mat = glm::rotate(rotation_mat, rotation.x, glm::vec3(1, 0, 0));
    rotation_mat = glm::rotate(rotation_mat, rotation.y, glm::vec3(0, 1, 0));
    rotation_mat = glm::rotate(rotation_mat, rotation.z, glm::vec3(0, 0, 1));
}

void GLCanvas::translateModel(double rot_x, double rot_y, double rot_z)
{
    translation += glm::vec3(-rot_y, -rot_x, 0) * TRANSLATION_SPEED;
    translation_mat = glm::mat4(1.0f);
    translation_mat = glm::translate(translation_mat, translation);
}

void GLCanvas::scaleModel(short step)
{
    scale_steps += step;
    double scale = glm::pow(2, scale_steps * SCALING_SPEED);
    scaling_mat = glm::mat4(1.0f);
    scaling_mat = glm::scale(scaling_mat, glm::vec3(scale, scale, 0));
    model_mat = scaling_mat * rotation_mat;
}

void GLCanvas::nextFrame(wxPaintEvent& evt)
{
    LOG_GL_ERRORS

    prepare3DViewport();
    shader_program->use();

    glm::mat4 view_mat = camera->getTransformationMatrix();
    glm::mat4 projection_mat = glm::perspective(30.0f, 1.0f, 0.01f, 100.0f);

    // setup textures
    if(heightmap_set) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_height_id); // todo error when no texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(GL_LINEAR));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(GL_LINEAR));

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture_normal_id); // todo error when no texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(GL_LINEAR));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(GL_LINEAR));

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, texture_color_id); // todo error when no texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(GL_LINEAR));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(GL_LINEAR));
    }

    // set up VAO
    glBindVertexArray(VAO);
    model_mat = glm::mat4(1.0f);
    model_mat = glm::scale(model_mat, glm::vec3(5.0f));
    model_mat = glm::translate(model_mat, glm::vec3(-0.5f, -0.5f, 0.0f));
    model_mat = glm::translate(model_mat, translation);
    model_mat = rotation_mat * model_mat;
    glUniformMatrix4fv(sh_uni_model, 1, GL_FALSE, glm::value_ptr(model_mat));
    glUniformMatrix4fv(sh_uni_view, 1, GL_FALSE, glm::value_ptr(view_mat));
    glUniformMatrix4fv(sh_uni_projection, 1, GL_FALSE, glm::value_ptr(projection_mat));

    glm::vec3 cameraPos = camera->getPosition();
//    glm::vec3 cameraPos(0,0,1);
    glUniform3f(glGetUniformLocation(shader_program->get_program_id(), "eyePos"), cameraPos.x, cameraPos.y, cameraPos.z);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//    glPatchParameteri(GL_PATCH_VERTICES, 3);
//    glDrawArrays(GL_PATCHES, 0, 6);
    glDrawElements(GL_TRIANGLES, getMeshIndicesCount(), GL_UNSIGNED_INT, nullptr);

    if(IS_GL_ERROR) throw except::OpenGL(except::ErrorType::DRAW, glErrorCode);

    glFlush();
    SwapBuffers();
}
