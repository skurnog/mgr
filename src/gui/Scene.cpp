#include "Scene.h"

#include <graphics/Camera.h>
#include <logic/Object.h>
#include <resources/GameObjectInitializer.h>
#include <glm/ext.hpp>

Scene::Scene(unsigned shaderId) {
    camera = std::make_unique<Camera>();
    gameObjectInitializer = std::make_unique<GameObjectInitializer>(shaderId);
}

Scene::~Scene() {}

std::vector<std::shared_ptr<Object>> &Scene::getSceneObjects() noexcept {
    return sceneObjects;
}

void Scene::cameraTranslate(const glm::vec3 &vector) const noexcept {
    camera->translate(vector);
}

void Scene::cameraRotate(double yaw, double pitch) const noexcept {
    camera->rotate(yaw, pitch);
}

const glm::mat4 &Scene::getCameraMatrix() const noexcept {
    return camera->getTransformation();
}

glm::vec3 Scene::getCameraPosition() const noexcept {
    return glm::normalize(camera->cameraFront + camera->position) * camera->position.length();
}

void Scene::update() {
    if(!cached_ld) {
        gameObjectInitializer->initialize(sceneObjects);
        cached_ld = true;
    } // todo NEW OBJECTS
}
