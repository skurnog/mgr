#include <gui/MainGUI.h>
#include <gpu/KernelRunner.h>

#include <resources/Heightmap.h>

#include <gui/PreviewWindow.h>
//#include <gui/PreviewWindowNoise.h>

//#define __WXMSW__
//#define _UNICODE

IMPLEMENT_APP(HelloWorldApp)

// This is executed upon startup, like 'main()' in non-wxWidgets programs.
bool HelloWorldApp::OnInit()
{
    Util::nextRunNumber();

    wxInitAllImageHandlers();

    // PREVIEW
//    PreviewWindowNoise *previewWindow = new PreviewWindowNoise(nullptr);
    PreviewWindow *previewWindow = new PreviewWindow(nullptr);
    previewWindow->CreateStatusBar();
    previewWindow->SetStatusText("TEST8");
    previewWindow->Show(true);
    SetTopWindow(previewWindow);
    return true;
}