///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug 20 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include <gui/PreviewWindowNoise.h>
#include <gpu/KernelRunner.h>

///////////////////////////////////////////////////////////////////////////

PreviewWindowNoise::PreviewWindowNoise( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
    this->SetSizeHints( wxSize( 500,-1 ), wxDefaultSize );

    wxBoxSizer* bSizer;
    bSizer = new wxBoxSizer( wxVERTICAL );

    imagePanel = new wxImagePanel(this);
    bSizer->Add(imagePanel, 5, wxEXPAND);

    wxBoxSizer* bSizer111;
    bSizer111 = new wxBoxSizer( wxHORIZONTAL );

    wxBoxSizer* box_parameters;
    box_parameters = new wxBoxSizer( wxVERTICAL );

    wxBoxSizer* bSizer11;
    bSizer11 = new wxBoxSizer( wxHORIZONTAL );

    m_staticText41 = new wxStaticText( this, wxID_ANY, wxT("Noise algorithm:"), wxDefaultPosition, wxDefaultSize, 0 );
    m_staticText41->Wrap( -1 );
    bSizer11->Add( m_staticText41, 0, wxALIGN_CENTER|wxALL, 5 );

    m_comboBox1 = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
    m_comboBox1->Append( wxT("Perlin CPU") );
    m_comboBox1->Append( wxT("Perlin GPU") );
    m_comboBox1->SetSelection( 0 );
    bSizer11->Add( m_comboBox1, 0, wxALIGN_CENTER|wxALL, 5 );


    box_parameters->Add( bSizer11, 1, wxALIGN_CENTER|wxALL, 5 );

    wxBoxSizer* bSizer8;
    bSizer8 = new wxBoxSizer( wxHORIZONTAL );

    wxBoxSizer* bSizer4;
    bSizer4 = new wxBoxSizer( wxVERTICAL );

    m_staticText5 = new wxStaticText( this, wxID_ANY, wxT("height"), wxDefaultPosition, wxDefaultSize, 0 );
    m_staticText5->Wrap( -1 );
    bSizer4->Add( m_staticText5, 0, wxALIGN_CENTER, 5 );

    spinCtrl_tex_h = new wxSpinCtrl( this, wxID_ANY, wxT("64"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 10000, 1 );
    spinCtrl_tex_h->SetMinSize( wxSize( 70,-1 ) );
    spinCtrl_tex_h->SetMaxSize( wxSize( 70,-1 ) );

    bSizer4->Add( spinCtrl_tex_h, 0, wxALL, 5 );


    bSizer8->Add( bSizer4, 1, wxALIGN_CENTER, 5 );

    wxBoxSizer* bSizer5;
    bSizer5 = new wxBoxSizer( wxVERTICAL );

    m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("width"), wxDefaultPosition, wxDefaultSize, 0 );
    m_staticText4->Wrap( -1 );
    bSizer5->Add( m_staticText4, 0, wxALIGN_CENTER, 5 );

    spinCtrl_tex_w = new wxSpinCtrl( this, wxID_ANY, wxT("64"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 10000, 1 );
    spinCtrl_tex_w->SetMinSize( wxSize( 70,-1 ) );
    spinCtrl_tex_w->SetMaxSize( wxSize( 70,-1 ) );

    bSizer5->Add( spinCtrl_tex_w, 0, wxALL, 5 );


    bSizer8->Add( bSizer5, 1, wxALIGN_CENTER, 5 );

    wxBoxSizer* bSizer6;
    bSizer6 = new wxBoxSizer( wxVERTICAL );

    m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("scale"), wxDefaultPosition, wxDefaultSize, 0 );
    m_staticText3->Wrap( -1 );
    bSizer6->Add( m_staticText3, 0, wxALIGN_CENTER, 5 );

    spinCtrlDouble_scale = new wxSpinCtrlDouble( this, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 5, 0.01 );
    spinCtrlDouble_scale->SetDigits( 3 );
    spinCtrlDouble_scale->SetMinSize( wxSize( 70,-1 ) );
    spinCtrlDouble_scale->SetMaxSize( wxSize( 70,-1 ) );

    bSizer6->Add( spinCtrlDouble_scale, 0, wxALL, 5 );


    bSizer8->Add( bSizer6, 1, wxALIGN_CENTER, 5 );


    box_parameters->Add( bSizer8, 1, wxALIGN_CENTER|wxALL, 5 );


    bSizer111->Add( box_parameters, 1, wxALIGN_CENTER|wxALL, 5 );

    wxBoxSizer* box_buttons;
    box_buttons = new wxBoxSizer( wxHORIZONTAL );

    wxBoxSizer* bSizer61;
    bSizer61 = new wxBoxSizer( wxVERTICAL );

    save_button = new wxButton( this, wxID_ANY, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer61->Add( save_button, 0, wxALIGN_CENTER, 5 );

    use_button = new wxButton( this, wxID_ANY, wxT("Use"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer61->Add( use_button, 0, wxALIGN_CENTER, 5 );


    box_buttons->Add( bSizer61, 1, wxALIGN_CENTER|wxALL, 5 );

    wxBoxSizer* bSizer71;
    bSizer71 = new wxBoxSizer( wxVERTICAL );

    checkBox_interactive = new wxCheckBox( this, wxID_ANY, wxT("Auto generate"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer71->Add( checkBox_interactive, 0, wxALIGN_CENTER|wxALIGN_LEFT, 5 );

    generate_button = new wxButton( this, wxID_ANY, wxT("Generate"), wxDefaultPosition, wxDefaultSize, 0 );
    bSizer71->Add( generate_button, 0, wxALIGN_CENTER|wxALL, 5 );


    box_buttons->Add( bSizer71, 1, wxALIGN_CENTER|wxALL, 5 );


    bSizer111->Add( box_buttons, 1, wxALIGN_CENTER|wxALL, 5 );


    bSizer->Add( bSizer111, 1, wxALL|wxEXPAND|wxSHAPED, 5 );


    // Bind event handlers
    use_button->Bind(wxEVT_BUTTON, &PreviewWindowNoise::OnUseButton, this);
    save_button->Bind(wxEVT_BUTTON, &PreviewWindowNoise::OnSaveButton, this);
    generate_button->Bind(wxEVT_BUTTON, &PreviewWindowNoise::OnGenerateButton, this);
    spinCtrl_tex_h->Bind(wxEVT_SPINCTRL, &PreviewWindowNoise::OnHeightSpin, this);
    spinCtrl_tex_w->Bind(wxEVT_SPINCTRL, &PreviewWindowNoise::OnWidthSpin, this);
    spinCtrlDouble_scale->Bind(wxEVT_SPINCTRLDOUBLE, &PreviewWindowNoise::OnScaleSpin, this);
    m_comboBox1->Bind(wxEVT_COMBOBOX, &PreviewWindowNoise::OnAlgorithmCombo, this);
    checkBox_interactive->Bind(wxEVT_CHECKBOX, &PreviewWindowNoise::OnAutoGenerateCheckbox, this);

    this->SetSizer( bSizer );
    this->Layout();

    this->Centre( wxBOTH );
}

PreviewWindowNoise::~PreviewWindowNoise()
{
}

void PreviewWindowNoise::generateNoise()
{
    int selection = this->m_comboBox1->GetSelection();

    auto w = this->spinCtrl_tex_w->GetValue();
    auto h = this->spinCtrl_tex_h->GetValue();
    auto scale = this->spinCtrlDouble_scale->GetValue();

    switch (selection) {
        case 0:
            current_noise = perlinNoiseCPU.noise2D(w, h, scale);
            current_noise_w = w;
            current_noise_h = h;
            imagePanel->setImage(w, h, current_noise);
            break;
        case 1:
            auto res = KernelRunner::runKernelPerlin(w, h, scale);
            std::vector<unsigned char> noise;
            for(float val: res) {
                noise.push_back((unsigned char)(val*255));
                noise.push_back((unsigned char)(val*255));
                noise.push_back((unsigned char)(val*255));
                noise.push_back(255);
            }
            imagePanel->setImage(w, h, noise);
            break;
    }
    imagePanel->repaint();
}

void PreviewWindowNoise::OnUseButton(wxCommandEvent & event)
{
    std::cout << "use" << std::endl << std::flush;
}

void PreviewWindowNoise::OnSaveButton(wxCommandEvent & event)
{
    wxFileDialog *saveDialog =
            new wxFileDialog(this, _("Save File As _?"), wxEmptyString, wxEmptyString,
                    _("Images (*.png)|*.png"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT, wxDefaultPosition);

    if (saveDialog->ShowModal() == wxID_OK) {
        std::string filePath = saveDialog->GetPath();
        ResourceManager::saveImage(filePath, image_t{current_noise, current_noise_w, current_noise_h});
    }

    saveDialog->Destroy();
}

void PreviewWindowNoise::OnGenerateButton(wxCommandEvent & event)
{
    generateNoise();
}

void PreviewWindowNoise::OnHeightSpin(wxCommandEvent & event)
{
    if(interactive) generateNoise();
    spinCtrl_tex_w->SetValue(spinCtrl_tex_h->GetValue());
}

void PreviewWindowNoise::OnWidthSpin(wxCommandEvent & event)
{
    if(interactive) generateNoise();
    spinCtrl_tex_h->SetValue(spinCtrl_tex_w->GetValue());
}

void PreviewWindowNoise::OnScaleSpin(wxCommandEvent & event)
{
    if(interactive) generateNoise();
}

void PreviewWindowNoise::OnAlgorithmCombo(wxCommandEvent & event)
{
    if(interactive) generateNoise();
}

void PreviewWindowNoise::OnAutoGenerateCheckbox(wxCommandEvent & event)
{
    interactive = checkBox_interactive->GetValue();
}
