///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Mar  8 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
//
// I had to! :(
///////////////////////////////////////////////////////////////////////////

#include <gui/PreviewWindow.h>

///////////////////////////////////////////////////////////////////////////

PreviewWindow::PreviewWindow( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
    this->SetSizeHints( wxDefaultSize, wxDefaultSize );

    wxBoxSizer* bSizer;
    bSizer = new wxBoxSizer( wxVERTICAL );

    int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};
    glCanvas = new GLCanvas((wxFrame*) this, args);
    bSizer->Add(glCanvas, 1, wxEXPAND);
    
    this->SetSizer( bSizer );
    this->Layout();

    this->Centre( wxBOTH );
}

PreviewWindow::~PreviewWindow()
{
}
