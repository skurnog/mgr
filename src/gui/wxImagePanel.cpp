#include <gui/wxImagePanel.h>

BEGIN_EVENT_TABLE(wxImagePanel, wxPanel)
    EVT_PAINT(wxImagePanel::paintEvent)
    EVT_SIZE(wxImagePanel::OnSize)
END_EVENT_TABLE()

wxImagePanel::wxImagePanel(wxFrame* parent) :
        wxPanel(parent)
{
    // Load a placeholder image
    image = new wxImage(RESOURCES_DIR "so.png", wxBITMAP_TYPE_PNG);
}

void wxImagePanel::paintEvent(wxPaintEvent & evt)
{
    wxPaintDC dc(this);
    render(dc);
}

void wxImagePanel::OnSize(wxSizeEvent& event){
    Refresh();
    event.Skip();
}

void wxImagePanel::render(wxDC&  dc)
{
    if(!image->IsOk())
        return;

    int neww, newh;
    dc.GetSize( &neww, &newh );

    if( neww != w || newh != h )
    {
        resized = wxBitmap( image->Scale( neww, newh /*, wxIMAGE_QUALITY_HIGH*/ ) );
        w = neww;
        h = newh;
        dc.DrawBitmap( resized, 0, 0, false );
    }else{
        dc.DrawBitmap( resized, 0, 0, false );
    }
}

void wxImagePanel::setImage(int width, int height, std::vector<unsigned char> image_bytes)
{
    // TODO: zrobić to jakoś noramalnie
    ResourceManager::saveImage(std::string(OUTPUTS_DIR "temp.png"), image_t{image_bytes, (unsigned)width, (unsigned)height});
    image->LoadFile(std::string(OUTPUTS_DIR "temp.png"));
}

void wxImagePanel::repaint()
{
    wxClientDC dc(m_parent);
    render(dc);
    m_parent->Refresh();
    m_parent->Update();
}
