#include "GraphicsEngine.h"

#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>
using namespace gl;

#include <glm/gtc/matrix_transform.hpp>

#include <io/Window.h>
#include <graphics/ShaderProgram.h>
#include <graphics/Scene.h>
#include <resources/model/Model.h>
#include <resources/model/Geometry.h>
#include <resources/model/Mesh.h>
#include <resources/texture/Texture.h>
#include <logic/Object.h>
#include <logger/loggers.h>
#include <exceptions/OpenGLException.h>
#include <assimp/camera.h>

GraphicsEngine::GraphicsEngine(Window *window) : window(window), mainShader(std::make_unique<ShaderProgram>()) {
    glbinding::Binding::initialize();

    init_shaders();
    init_textures();

    glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE);
    glEnable(GL_DEPTH_TEST);
}

GraphicsEngine::~GraphicsEngine() {}

/**
 * Renders next frame. Object contained by Scene will be rendered from the point of view of current Camera.
 */
void GraphicsEngine::nextFrame() {
    LOG_GL_ERRORS

    window->getGeometry(&window_width, &window_height);
    glViewport(0, 0, window_width, window_height);

    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);

    // world transformations
    projection_mat = glm::perspective(30.0f, 1.0f, 0.01f, 100.0f);
    mainShader->use();

    glUniformMatrix4fv(sh_uni_view, 1, GL_FALSE, glm::value_ptr(scene->getCameraMatrix()));
    glUniformMatrix4fv(sh_uni_projection, 1, GL_FALSE, glm::value_ptr(projection_mat));

    const glm::vec3& camera_pos = scene->getCameraPosition();
    glUniform3f(sh_uni_light_pos, camera_pos.x, camera_pos.y, camera_pos.z);

    for(auto it = scene->getSceneObjects().begin(); it != scene->getSceneObjects().end(); ++it) {
        LOG_GL_ERRORS

        std::shared_ptr<Object>& sceneObject = *it;
        if(!sceneObject->isValid()) continue; // todo log invalid object

        glUniform1f(sh_uni_ambient_intensity, sceneObject->getModel().getAmbientIntensity());

        for(const Mesh& mesh: sceneObject->getModel().getGeometry().getMeshes()) { // todo error when no geometry
            glBindVertexArray(mesh.getVAO());
            glUniformMatrix4fv(sh_uni_model, 1, GL_FALSE, glm::value_ptr(sceneObject->getTransformationMatrix()));

            // models drawing
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, sceneObject->getModel().getTexture().getTextureId()); // todo error when no texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(GL_LINEAR));
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(GL_LINEAR));

            glDrawElements(GL_TRIANGLES, mesh.getElementsCount(), GL_UNSIGNED_INT, 0);
            glBindVertexArray(0);
            glBindTexture(GL_TEXTURE_2D, 0);

            if (glGetError() != GL_NONE) throw ex::OpenGLException(ErrorType::FRAME_RENDER);
        }
    }
    window->draw();
}

/**
 * Shaders initialization. Needs to be called before first draw call.
 */
void GraphicsEngine::init_shaders() {
    LOG_GL_ERRORS

    mainShader->init();
    mainShader->load_shader("../shaders/main.vert", GL_VERTEX_SHADER);
    mainShader->load_shader("../shaders/main.frag", GL_FRAGMENT_SHADER);
    mainShader->link();

    sh_uni_model = glGetUniformLocation(mainShader->program_id, "modelMat");
    sh_uni_view = glGetUniformLocation(mainShader->program_id, "viewMat");
    sh_uni_projection = glGetUniformLocation(mainShader->program_id, "projectionMat");
    sh_uni_ambient_intensity = glGetUniformLocation(mainShader->program_id, "ambientIntensity");
    sh_uni_light_pos = glGetUniformLocation(mainShader->program_id, "lightPos");

    if (glGetError() != GL_NO_ERROR) throw ex::OpenGLException(ErrorType::SHADER);
    else {
        scene = std::make_shared<Scene>(mainShader->program_id);
        LOG_INFO  << "Shaders initialized";
    }
}

/**
 * Texture units initialization. Needs to be called before first draw call.
 */
void GraphicsEngine::init_textures() {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(GL_LINEAR));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(GL_LINEAR));
}

std::shared_ptr<Scene> GraphicsEngine::getScene() const noexcept {
    return scene;
}
