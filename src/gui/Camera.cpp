#include <gui/Camera.h>
#include <iostream>


Camera::Camera() : rotationDegrees(0.0f), cameraUp(0.0f, 1.0f, 0.0f), cameraFront(0.0f, 0.0f, -1.0f), cameraRight(1.0f, 0.0f, 0.0f) {
    translate(glm::vec3(0.0f, 0.0f, 1.0f));
    distanceFromOrigin = 2;
    rotateAroundOrigin(0, 0);
}

const glm::mat4& Camera::getTransformationMatrix() noexcept {
//    transformationMatrix = glm::lookAt(position, position + cameraFront, cameraUp);
    transformationMatrix = glm::lookAt(position, glm::vec3(0.0f), cameraUp);
    return transformationMatrix;
}

glm::vec3 Camera::getPosition() const noexcept
{
    return glm::normalize(cameraFront - position * glm::vec3(position.length()).x);
}

/*
 * Move camera, move target with it.
 */
void Camera::translate(glm::vec3 const& vector) noexcept {
    if(translationEnabled) {
        position = position + cameraRight * vector.x;
        position = position + cameraFront * vector.z;
    }
}

void Camera::rotate(double _yaw, double _pitch) noexcept {
    yaw += _yaw;
    pitch += _pitch;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front(0.0f);
    front.x = static_cast<float>(glm::cos(glm::radians(yaw)) * glm::cos(glm::radians(pitch)));
    front.y = static_cast<float>(glm::sin(glm::radians(pitch)));
    front.z = static_cast<float>(glm::sin(glm::radians(yaw)) * glm::cos(glm::radians(pitch)));

    cameraFront = glm::normalize(front);
    cameraRight = glm::normalize(glm::cross(cameraFront, cameraUp));
}

void Camera::rotateAroundOrigin(float stepsX, float stepsY) {
    std::cout << stepsX << " " << stepsY << std::endl << std::flush;
    float newDegreeX = rotationDegrees.x + ROTATION_SPEED * stepsX;
    if(newDegreeX > ROTATION_X_MAX) newDegreeX = ROTATION_X_MAX;
    if(newDegreeX < ROTATION_X_MIN) newDegreeX = ROTATION_X_MIN;
    float newDegreeY = rotationDegrees.y + ROTATION_SPEED * stepsY;
    if(newDegreeY > ROTATION_Y_MAX) newDegreeY = ROTATION_Y_MAX;
    if(newDegreeY < ROTATION_Y_MIN) newDegreeY = ROTATION_Y_MIN;
    rotationDegrees = glm::vec2(newDegreeX, newDegreeY);

    updatePosition();
    printVec(rotationDegrees, "rot");
    printVec(position, "pos");
}

void Camera::updatePosition() {
    double newPositionY = distanceFromOrigin * std::cos(glm::radians(rotationDegrees.x)) * std::cos(
            glm::radians(rotationDegrees.y));
    double newPositionZ = distanceFromOrigin * std::cos(glm::radians(rotationDegrees.x)) * std::sin(
            glm::radians(rotationDegrees.y));
    double newPositionX = distanceFromOrigin * std::sin(glm::radians(rotationDegrees.x));
    position = glm::vec3(newPositionX, newPositionY, newPositionZ);
}

void Camera::zoom(float distanceDelta) {
    distanceFromOrigin += distanceDelta * ZOOM_SPEED;
    if(distanceFromOrigin > DISTANCE_MAX) distanceFromOrigin = DISTANCE_MAX;
    if(distanceFromOrigin < DISTANCE_MIN) distanceFromOrigin = DISTANCE_MIN;
    position = glm::normalize(position) * distanceFromOrigin;
//    updatePosition();
//    std::cout << distanceFromOrigin << std::endl << std::flush;
}

void Camera::printVec(const glm::vec3 &vector, const std::string& name) const {
    std::cout << name << ": (" << vector.x << ", " << vector.y << ", " << vector.z << ")" << std::endl << std::flush;
}

void Camera::printVec(const glm::vec2 &vector, const std::string& name) const {
    std::cout << name << ": (" << vector.x << ", " << vector.y << ")" << std::endl << std::flush;
}