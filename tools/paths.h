//
// Created by norbert on 29.03.18.
//

#ifndef MGR_PATHS_H
#define MGR_PATHS_H

#ifndef EXECUTABLE_DIR
#define EXECUTABLE_DIR ""
#endif

#define LOG_DIR SOURCES_DIR "logs/"
#define RESOURCES_DIR SOURCES_DIR "resources/"
#define KERNELS_DIR SOURCES_DIR "kernels/"
#define SHADERS_DIR SOURCES_DIR "shaders/"
#define OUTPUTS_DIR SOURCES_DIR "outputs/"


#endif //MGR_PATHS_H
